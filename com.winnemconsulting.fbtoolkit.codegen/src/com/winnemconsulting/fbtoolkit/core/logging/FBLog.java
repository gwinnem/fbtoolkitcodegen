/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.core.logging;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.winnemconsulting.fbtoolkit.Activator;

/**
 * Centralized FBLog handler for fbtoolkit. This class is writing to the Error
 * Log View in Eclipse
 * 
 * @author Geirr Winnem
 */
public class FBLog {

	/**
	 * Default Constructor.
	 */
	public FBLog() {
		// Do not remove this constructor without it other plugins can not see
		// this class
	}

	/**
	 * Creating a new log info message
	 * 
	 * @param message
	 *            String Message to write to log
	 */
	public static void logInfo(final String message) {
		log(IStatus.INFO, Activator.PLUGIN_ID, IStatus.OK, message, null);
	}

	/**
	 * Creating a new log info message
	 * 
	 * @param message
	 *            String Message to write to log
	 * @param pluginId
	 *            String the unique identifier of the relevant plug-in
	 */
	public static void logInfo(final String pluginId, final String message) {
		log(IStatus.INFO, pluginId, IStatus.OK, message, null);
	}

	/**
	 * Creating a new log error message.
	 * <p>
	 * This method must only be from core plugin, because it uses the pluginId
	 * from that plugin.
	 * </p>
	 * 
	 * @param exception
	 *            Throwable a low-level exception, or null if not applicable
	 * 
	 * @see java.lang.Throwable
	 */
	public static void logError(final Throwable exception) {
		logError("Unexpected Exception", exception, Activator.PLUGIN_ID);
	}

	/**
	 * Creating a new log error message
	 * 
	 * @param pluginId
	 *            String the unique identifier of the relevant plug-in
	 * 
	 * @param exception
	 *            Throwable a low-level exception, or null if not applicable
	 * 
	 * @see java.lang.Throwable
	 */
	public static void logError(final String pluginId, final Throwable exception) {
		logError("Unexpected Exception", exception, pluginId);
	}

	/**
	 * Creating a new log error message
	 * 
	 * @param message
	 *            String the message to use
	 * @param exception
	 *            Throwable a low-level exception, or null if not applicable
	 * @param pluginId
	 *            String the unique identifier of the relevant plug-in
	 * 
	 * @see java.lang.Throwable
	 */
	public static void logError(final String message,
			final Throwable exception, final String pluginId) {
		log(IStatus.ERROR, pluginId, IStatus.OK, message, exception);
	}

	/**
	 * Main worker
	 * 
	 * @param status
	 *            IStatus object
	 * @see org.eclipse.core.runtime.IStatus
	 */
	private static void log(final IStatus status) {
		Activator.getDefault().getLog().log(status);
	}

	/**
	 * Creating a new log entry
	 * 
	 * @param severity
	 *            Int the severity; one of Status.OK, Status.ERROR, Status.INFO,
	 *            Status.WARNING, or Status.CANCEL
	 * @param pluginId
	 *            String the unique identifier of the relevant plug-in
	 * @param code
	 *            Int the plug-in-specific status code, or Status.OK
	 * @param message
	 *            String a human-readable message, localized to the current
	 *            locale
	 * @param exception
	 *            Throwable a low-level exception, or null if not applicable
	 * @return a new Status object
	 * 
	 * @see java.lang.Throwable
	 * @see org.eclipse.core.runtime.Status
	 */
	private static void log(final int severity, final String pluginId,
			final int code, final String message, final Throwable exception) {
		log(createStatus(severity, pluginId, code, message, exception));
	}

	/**
	 * Creating a new Status object
	 * 
	 * @param severity
	 *            Int the severity; one of Status.OK, Status.ERROR, Status.INFO,
	 *            Status.WARNING, or Status.CANCEL
	 * @param pluginId
	 *            String the unique identifier of the relevant plug-in
	 * @param code
	 *            Int the plug-in-specific status code, or Status.OK
	 * @param message
	 *            String a human-readable message, localized to the current
	 *            locale
	 * @param exception
	 *            Throwable a low-level exception, or null if not applicable
	 * @return a new Status object
	 * 
	 * @see java.lang.Throwable
	 * @see org.eclipse.core.runtime.th
	 */
	private static IStatus createStatus(final int severity,
			final String pluginId, final int code, final String message,
			final Throwable exception) {
		return new Status(severity, pluginId, code, message, exception);
	}

}
