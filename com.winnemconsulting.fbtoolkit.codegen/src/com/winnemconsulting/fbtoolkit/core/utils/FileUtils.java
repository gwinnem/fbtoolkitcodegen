/*
Copyright (c) 2011-
Winnem Consulting
eMail: support@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.winnemconsulting.fbtoolkit.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Common utils for file manipulation.
 * 
 * @author Geirr Winnem
 */
public class FileUtils {

	public static final int OVERRIDE_NEVER = 0;

	public static final int OVERRIDE_UPDATE = 1;

	public static final int OVERRIDE_ALWAYS = 2;

	public static final int ERROR_STATUS_CODE = 123;

	/**
	 * Extracting filename
	 * 
	 * @param completePath
	 * @return String filename extracted from the completePath
	 */
	public static String getFileName(final String completePath) {
		String retVal = "";
		if (completePath.length() != 0) {
			final File tmpFile = new File(completePath);
			retVal = tmpFile.getName();
		}
		return retVal;
	}

	/**
	 * Copy file single file. The parameters <code>srcFile</code> and
	 * <code>destFile</code> can't be directory.
	 * 
	 * @param srcFile
	 * @param destFile
	 * @param override
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void copyFile(final File srcFile, final File destFile,
			final int override) throws FileNotFoundException, IOException {
		if (override == OVERRIDE_NEVER && destFile.exists()) {
			return;
		}
		if (override == OVERRIDE_UPDATE
				&& destFile.lastModified() > srcFile.lastModified()) {
			return;
		}

		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		try {
			fileInputStream = new FileInputStream(srcFile);
			fileOutputStream = new FileOutputStream(destFile);
			fileInputStream.getChannel().transferTo(0, srcFile.length(),
					fileOutputStream.getChannel());
		} finally {
			close(fileInputStream);
			close(fileOutputStream);
		}
	}

	/**
	 * Recursive copy all files from specified directory.
	 * 
	 * @param srcDir
	 * @param destDir
	 * @param override
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void copyFiles(final File srcDir, final File destDir,
			final int override) throws FileNotFoundException, IOException {
		copyFiles(srcDir, srcDir.list(), destDir, override);
	}

	/**
	 * Recursive copy list of files from specified directory.
	 * 
	 * @param srcDir
	 * @param files
	 * @param destDir
	 * @param override
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void copyFiles(final File srcDir, final String[] files,
			final File destDir, final int override)
			throws FileNotFoundException, IOException {
		destDir.mkdirs();
		for (int index = 0; index < files.length; index++) {
			final String fileName = files[index];
			final File srcFile = new File(srcDir, fileName);
			if (srcFile.isDirectory()) {
				if (fileName.equals("CVS"))
					continue;
				copyFiles(srcFile, new File(destDir, fileName), override);
				continue;
			}
			copyFile(srcFile, new File(destDir, fileName), override);
		}
	}

	/**
	 * Close specified input stream and catch and ignore all Throwable.
	 * 
	 * @param stream
	 *            InputStream to close
	 */
	public static void close(final InputStream stream) {
		try {
			stream.close();
		} catch (final Throwable thIgnore) {
		}
	}

	/**
	 * Close specified output stream and catch and ignore all Throwable.
	 * 
	 * @param stream
	 *            OutputStream to close
	 */
	public static void close(final OutputStream stream) {
		try {
			stream.close();
		} catch (final Throwable thIgnore) {
		}
	}
}
