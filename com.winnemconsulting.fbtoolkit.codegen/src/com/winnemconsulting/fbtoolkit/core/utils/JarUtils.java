package com.winnemconsulting.fbtoolkit.core.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.osgi.framework.Bundle;

import com.winnemconsulting.fbtoolkit.Activator;

/**
 * Utility class for misc jar operations
 * 
 * @author Geirr Winnem
 * 
 */
public class JarUtils {
	/**
	 * Extracting a file from a jar bundle
	 * 
	 * @param srcFile
	 *            Source url
	 * @param destFile
	 *            Destination
	 * @throws IOException 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws CoreException
	 */
	public static void extractFile(final String srcFile, final String destFile) throws IOException {

		Bundle pluginBundle = Activator.getDefault().getBundle();
		InputStream inputFile = FileLocator.openStream(pluginBundle, new Path(srcFile), false);
		File outFile = new File(destFile);
		OutputStream outStream = new FileOutputStream(outFile);
		byte buf[] = new byte[1024];
		int len;
		while((len=inputFile.read(buf))>0){
			outStream.write(buf,0,len);
		}
		outStream.close();
		inputFile.close();
	}
}
