/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.core.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Helper class for reading .actionScriptProperties file
 * 
 * @author Geirr Winnem
 */
public class ActionScriptPropertyReader {

	private DocumentBuilder documentBuilder;
	private DocumentBuilderFactory documentFactory;

	private Document document;

	/**
	 * Absolute path to project root on disk
	 */
	private String projectAbsolutePath = "";

	/**
	 * Name of file that is set as main application
	 */
	private String mainApplication = "";
	/**
	 * Flex application version
	 */
	private String mainApplicationVersion = "";
	/**
	 * Project source folder
	 */
	private String mainSourceFolderPath = "";
	/**
	 * Project library folder
	 */
	private final String mainLibraryPath = "";

	/**
	 * Name of property file to read
	 */
	private final static String PROPERTY_FILE = ".actionScriptProperties"; //$NON-NLS-1$

	/**
	 * Constructor
	 */
	public ActionScriptPropertyReader() {
		super();
		try {
			documentFactory = javax.xml.parsers.DocumentBuilderFactory
					.newInstance();
			documentFactory.setIgnoringComments(true);
			documentFactory.setIgnoringElementContentWhitespace(true);
			documentFactory.setCoalescing(true);
			documentBuilder = documentFactory.newDocumentBuilder();
		} catch (ParserConfigurationException exception) {
			FBLog.logError("ActionScriptPropertyReader", exception);
		}
	}

	/**
	 * Parsing property file
	 * 
	 * @param activeProject
	 *            Project user is working on
	 * @return True if file is parsed without errors
	 */
	public Boolean parsePropertyFile(final String projectRoot) {
		setProjectAbsolutePath(projectRoot);
		final File propertyFile = new File(getPropertyFileName());
		if (!propertyFile.exists()) {
			return false;
		}
		try {
			document = documentBuilder.parse(propertyFile);
		} catch (SAXException exception) {
			FBLog.logError("ActionScriptPropertyReader", exception);
			return false;
		} catch (IOException exception) {
			FBLog.logError("ActionScriptPropertyReader", exception);
			return false;
		}
		return true;
	}

	/**
	 * @return Full path to property file
	 */
	private String getPropertyFileName() {
		String retVal = getProjectAbsolutePath() + System.getProperty("file.separator")
				+ PROPERTY_FILE;
		
		return retVal.replaceAll("\\\\", "/");
	}

	/**
	 * @param projectAbsolutePath
	 *            the projectAbsolutePath to set
	 */
	public void setProjectAbsolutePath(String projectAbsolutePath) {
		this.projectAbsolutePath = projectAbsolutePath;
	}

	/**
	 * @return the projectAbsolutePath
	 */
	public String getProjectAbsolutePath() {
		return projectAbsolutePath;
	}

	/**
	 * @return the mainApplication
	 */
	public String getMainApplication() {
		return mainApplication;
	}

	/**
	 * @return the mainApplicationVersion
	 */
	public String getMainApplicationVersion() {
		return mainApplicationVersion;
	}

	/**
	 * @return the mainSourceFolderPath
	 */
	public String getMainSourceFolderPath() {
		return mainSourceFolderPath;
	}

	/**
	 * @return the mainLibraryPath
	 */
	public String getMainLibraryPath() {
		return mainLibraryPath;
	}

	/*
	 * Example file content
	 * 
	 * <actionScriptProperties mainApplicationPath="AirCodeGenerator.mxml"
	 * version="3"> <compiler additionalCompilerArguments="-locale en_US"
	 * copyDependentFiles="true" enableModuleDebug="true"
	 * generateAccessible="false" htmlExpressInstall="true" htmlGenerate="false"
	 * htmlHistoryManagement="false" htmlPlayerVersion="9.0.124"
	 * htmlPlayerVersionCheck="true" outputFolderPath="bin-debug"
	 * sourceFolderPath="src" strict="true" useApolloConfig="true"
	 * verifyDigests="true" warn="true"> <compilerSourcePath/> <libraryPath
	 * defaultLinkType="1"> <libraryPathEntry kind="4" path=""/>
	 * <libraryPathEntry kind="1" linkType="1" path="libs"/> <libraryPathEntry
	 * kind="3" linkType="1"
	 * path="/AirCodeGeneratorApi/bin/AirCodeGeneratorApi.swc"
	 * useDefaultLinkType="false"/> </libraryPath> <sourceAttachmentPath/>
	 * </compiler> <applications> <application path="AirCodeGenerator.mxml"/>
	 * </applications> <modules/> <buildCSSFiles/> </actionScriptProperties>
	 */

	/**
	 * Loading properties from file
	 */
	public void loadProperties() {
		getActionscriptProperties();
	}

	/**
	 * @return
	 */
	private Boolean getActionscriptProperties() {
		try {
			NodeList nodes = document
					.getElementsByTagName("actionScriptProperties");

			if (nodes.getLength() == 0) {
				return false;
			}

			Node workingNode = nodes.item(0);

			if (workingNode.getNodeName().equalsIgnoreCase(
					"actionScriptProperties")) {
				NamedNodeMap attributes = workingNode.getAttributes();
				workingNode = attributes.getNamedItem("mainApplicationPath");
				if (workingNode != null) {
					mainApplication = workingNode.getNodeValue();
				}
				workingNode = attributes.getNamedItem("version");
				if (!workingNode.equals(null)) {
					mainApplicationVersion = workingNode.getNodeValue();
				}

				nodes = document.getElementsByTagName("compiler");

				if (nodes.getLength() == 0) {
					return false;
				}

				workingNode = nodes.item(0);
				if (workingNode.getNodeName().equalsIgnoreCase("compiler")) {
					attributes = workingNode.getAttributes();
					workingNode = attributes.getNamedItem("sourceFolderPath");
					if (!workingNode.equals(null)) {
						mainSourceFolderPath = workingNode.getNodeValue();
					}
				}
			}
		} catch (NullPointerException exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
			return false;
		}
		return true;
	}
}
