/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.texteditor.ITextEditor;

import com.winnemconsulting.fbtoolkit.codegen.utils.Encloser;

/**
 * Base class for inserting code templates
 */
public class GenericEncloserAction extends Encloser implements
		IWorkbenchWindowActionDelegate, IEditorActionDelegate {
	protected transient ITextEditor editor = null;
	protected transient String start = "";
	protected transient String end = "";

	/**
	 * Constructor
	 */
	public GenericEncloserAction() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param startCode
	 * @param endCode
	 */
	public GenericEncloserAction(final String startCode, final String endCode) {
		super();
		setEnclosingStrings(startCode, endCode);
	}

	/**
	 * @param startCode
	 * @param endCode
	 */
	public final void setEnclosingStrings(final String startCode,
			final String endCode) {
		this.start = startCode;
		this.end = endCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IEditorActionDelegate#setActiveEditor(org.eclipse.jface
	 * .action.IAction, org.eclipse.ui.IEditorPart)
	 */
	public void setActiveEditor(final IAction action,
			final IEditorPart targetEditor) {
		if (targetEditor instanceof ITextEditor) {
			editor = (ITextEditor) targetEditor;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(final IAction action) {

		if (editor != null && editor.isEditable()) {
			final IDocument doc = editor.getDocumentProvider().getDocument(
					editor.getEditorInput());
			final ISelection sel = editor.getSelectionProvider().getSelection();
			this.enclose(doc, (ITextSelection) sel, start, end);

			// move the cursor to before the endCode of the new insert
			int offset = ((ITextSelection) sel).getOffset();
			offset += ((ITextSelection) sel).getLength();
			offset += start.length();

			editor.setHighlightRange(offset, 0, true);
			// Once we've moved the cursor we need to reset the highlight range
			// otherwise on the next insert, the cursor won't move.
			editor.resetHighlightRange();

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action
	 * .IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(final IAction action,
			final ISelection selection) {
		if (editor != null) {
			setActiveEditor(null, editor.getSite().getPage().getActiveEditor());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose() {
		// Not used
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.ui.IWorkbenchWindowActionDelegate#init(org.eclipse.ui.
	 * IWorkbenchWindow)
	 */
	public void init(final IWorkbenchWindow window) {
		final IEditorPart activeEditor = window.getActivePage()
				.getActiveEditor();
		if (activeEditor instanceof ITextEditor) {
			editor = (ITextEditor) activeEditor;
		}

	}
}
