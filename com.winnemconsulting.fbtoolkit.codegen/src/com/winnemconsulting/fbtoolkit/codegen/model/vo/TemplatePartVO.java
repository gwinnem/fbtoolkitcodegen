/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.model.vo;

/**
 * 
 * This is a definition of a local template. This is what the TemplateReader can
 * return
 * 
 * @author Geirr Winnem
 * 
 */
public class TemplatePartVO {

	/**
	 * Template description
	 */
	private String description;
	/**
	 * Code in template
	 */
	private String codeBlock;
	/**
	 * Template name
	 */
	private String name;

	/**
	 * Set Template description
	 * 
	 * @param templateDescription
	 */
	public void setDescription(final String templateDescription) {
		this.description = templateDescription;

	}

	/**
	 * Template description
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set Template code block
	 * 
	 * @param templateCodeBlock
	 */
	public void setCodeBlock(final String templateCodeBlock) {
		this.codeBlock = templateCodeBlock;

	}

	/**
	 * Template code block
	 * 
	 * @return code block
	 */
	public String getCodeBlock() {
		return codeBlock;
	}

	/**
	 * Set Template name
	 * 
	 * @param name
	 */
	public void setName(final String name) {
		this.name = name;

	}

	/**
	 * Template name
	 * 
	 * @return name of template
	 */
	public String getName() {
		return name;
	}
}
