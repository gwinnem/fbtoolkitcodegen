/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.model.vo;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;

/**
 * Value object used in CodegenSmartDialog and CodegenDialog when a user input
 * is needed
 * 
 * @author Geirr Winnem
 */
public class TemplateVarItemVO {
	private final String name;
	private final String[] values;
	private String replacement;
	private final String original;
	private Combo combo;
	private Text text;

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param values
	 * @param original
	 */
	public TemplateVarItemVO(final String name, final String[] values,
			final String original) {
		this.name = name;
		this.values = values;
		this.original = original;
		this.replacement = original;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return values
	 */
	public String[] getValues() {
		return this.values;
	}

	/**
	 * @return original value
	 */
	public String getOriginal() {
		return this.original;
	}

	/**
	 * @return replacement value
	 */
	public String getReplacement() {
		return this.replacement;
	}

	/**
	 * @param combo
	 */
	public void setCombo(final Combo combo) {
		this.combo = combo;
	}

	/**
	 * @param text
	 */
	public void setText(final Text text) {
		this.text = text;
	}

	/**
	 * Setting replacement text
	 */
	public void setReplacement() {
		if (combo != null) {
			this.replacement = combo.getText();
		} else if (text != null) {
			this.replacement = text.getText();
		}
	}

}
