/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.views.templates;

import java.io.File;
import java.io.FileFilter;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import com.winnemconsulting.fbtoolkit.codegen.views.TemplatesView;

/**
 * Filter used in TemplatesView
 * 
 * @author Geirr Winnem
 * 
 */
class TemplateFileFilter implements FileFilter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	public boolean accept(final File file) {
		final String sflower = file.getAbsoluteFile().toString().toLowerCase();
		if (sflower.endsWith(TemplatesView.TEMPLATE_EXT)
				|| (file.isDirectory() && !file.getName().startsWith("."))) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.FileFilter#accept(java.io.File)
	 */
	// public boolean accept(final File fileName) {
	// final String sflower =
	// fileName.getAbsoluteFile().toString().toLowerCase();
	// if (sflower.endsWith(TemplatesView.TEMPLATE_EXT)) {
	// return true;
	// }
	// return false;
	// }
	/**
	 * @return String constant value of File filter
	 */
	// public String getDescription() {
	// return "File filter";
	// }
}

/**
 * Responsible for populating templateview
 * 
 * @author Geirr Winnem
 * @see ITreeContentProvider
 */
public class TemplatesViewContentProvider implements ITreeContentProvider {
	protected static Object[] emptyArray = new Object[0];
	protected static FileFilter templateFileFilter = new TemplateFileFilter();
	protected TreeViewer viewer;
	protected File rootdir;

	/**
	 * Constructor
	 * 
	 * @param root
	 *            directory
	 */
	public TemplatesViewContentProvider(final File root) {
		rootdir = root;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface
	 * .viewers.Viewer, java.lang.Object, java.lang.Object)
	 */
	public void inputChanged(final Viewer viewer, final Object oldInput,
			final Object newInput) {
		this.viewer = (TreeViewer) viewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.
	 * Object)
	 */
	public Object[] getChildren(final Object parentElement) {
		if (parentElement instanceof File) {
			if (((File) parentElement).isDirectory()) {
				if (parentElement.equals(rootdir)) {
					Object[] files = ((File) parentElement)
							.listFiles(templateFileFilter);
					return files;

				} else {
					final File[] files = ((File) parentElement)
							.listFiles(templateFileFilter);
					return files;
				}
			}
		}
		return emptyArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object
	 * )
	 */
	public Object getParent(final Object element) {
		if (element instanceof File && !element.equals(rootdir)) {
			return ((File) element).getParentFile();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.
	 * Object)
	 */
	public boolean hasChildren(final Object element) {
		return getChildren(element).length > 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java
	 * .lang.Object)
	 */
	public Object[] getElements(final Object inputElement) {
		return getChildren(inputElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {
		// NONPMD
	}
}
