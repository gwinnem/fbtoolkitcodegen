/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.views.templates;

import java.io.File;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;

/**
 * LabelProvider for icons in TemplatesView
 * 
 * @author Geirr Winnem
 * 
 */
public class TemplatesViewIcons extends LabelProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(final Object element) {
		if (element instanceof File) {
			if (!((File) element).isDirectory()) {
				String fileName = ((File) element).getName();
				if (fileName.endsWith(".xml") || fileName.endsWith(".XML")) {
					return IconResources.get(IconResources.TEMPLATE_FORM);
				}
			} else {
				return IconResources.get(IconResources.FOLDER);
			}

		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object element) {
		// if this is a file
		if (element instanceof File) {
			final String filename = ((File) element).getName();
			if (!((File) element).isDirectory()) {
				return filename.substring(0, filename.length() - 4);
			} else {
				return filename;
			}
		}
		return element.toString();
	}

	/**
	 * @param element
	 * @return
	 */
	protected RuntimeException unknownElement(final Object element) {
		return new RuntimeException("Unknown type of element in tree of type "
				+ element.getClass().getName());// NOPMD
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.BaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {// NOPMD
	}
}
