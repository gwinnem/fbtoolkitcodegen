/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.views;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.part.ViewPart;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.actions.GenericEncloserAction;
import com.winnemconsulting.fbtoolkit.codegen.dialogs.NewFolderDialog;
import com.winnemconsulting.fbtoolkit.codegen.dialogs.TemplateFileDialog;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceConstants;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceManager;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceUtils;
import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;
import com.winnemconsulting.fbtoolkit.codegen.utils.MsgDialogs;
import com.winnemconsulting.fbtoolkit.codegen.utils.PlatformUtils;
import com.winnemconsulting.fbtoolkit.codegen.utils.PropertyFileReader;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateReader;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateVarParser;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateWriter;
import com.winnemconsulting.fbtoolkit.codegen.views.templates.TemplatesViewContentProvider;
import com.winnemconsulting.fbtoolkit.codegen.views.templates.TemplatesViewDoubleClickListener;
import com.winnemconsulting.fbtoolkit.codegen.views.templates.TemplatesViewIcons;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * WorkBench view displaying codegen templates.
 * 
 * @author Geirr Winnem
 * 
 * @see ViewPart
 * @see IPropertyChangeListener
 */
public class TemplatesView extends ViewPart implements IPropertyChangeListener {
	/**
	 * Identifier for this view
	 */
	public static final String				VIEW_NAME		= "com.winnemconsulting.fbtoolkit.codegen.codetemplates";	//$NON-NLS-1$

	/**
	 * Identifier for template type.
	 */
	public static final String				TEMPLATE_TYPE	= "FBToolkit";												//$NON-NLS-1$

	/**
	 * File extension for template file.
	 */
	public static final String				TEMPLATE_EXT	= "xml";													//$NON-NLS-1$

	protected TreeViewer					treeViewer;
	protected Text							preview;
	protected Label							previewLabel;
	protected LabelProvider					labelProvider;

	protected static IPath					codeTemplatesPath;
	private static GenericEncloserAction	tmpAction;

	private static TemplateReader			templateReader;
	private String							templateType;

	MenuManager								menuMgr;
	protected Action						refreshViewAction;
	protected Action						insertAction;
	protected Action						createFolderAction;
	protected Action						createTemplateAction;
	protected Action						editTemplateAction;
	protected Action						deleteTemplateAction;
	protected Action						deleteFolderAction;

	/** the root directory */
	protected File							rootFolder;

	private final PreferenceManager			preferenceManager;

	/**
	 * Constructor.
	 */
	public TemplatesView() {
		super();

		preferenceManager = Activator.getDefault().getPreferenceManager();

		templateType = TEMPLATE_TYPE;

		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(this);
		preferenceManager.addPropertyChangeListener(this);
		codeTemplatesPath = new Path(PreferenceUtils.getTemplatePath());

		if (tmpAction == null) {
			tmpAction = new GenericEncloserAction();
		}
		if (templateReader == null) {
			templateReader = new TemplateReader();
		}
	}

	/*
	 * @see IWorkbenchPart#createPartControl(Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		{
			SashForm sashForm = new SashForm(container, SWT.BORDER | SWT.SMOOTH | SWT.VERTICAL);

			Composite topform = new Composite(sashForm, SWT.BORDER);

			GridLayout gl_topform = new GridLayout(1, false);
			gl_topform.verticalSpacing = 1;
			gl_topform.horizontalSpacing = 1;
			topform.setLayout(gl_topform);

			treeViewer = new TreeViewer(topform, SWT.BORDER);
			treeViewer.setContentProvider(new TemplatesViewContentProvider(getRootInput()));
			labelProvider = new TemplatesViewIcons();
			treeViewer.setLabelProvider(labelProvider);

			treeViewer.setUseHashlookup(true);

			Tree tree = treeViewer.getTree();
			tree.setSortDirection(SWT.DOWN);
			GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
			gd_tree.horizontalIndent = 2;
			tree.setLayoutData(gd_tree);

			Composite bottomForm = new Composite(sashForm, SWT.BORDER);
			bottomForm.setLayout(new GridLayout(1, false));

			Label lblPreview = new Label(bottomForm, SWT.NONE);
			lblPreview.setText("Preview:");

			preview = new Text(bottomForm, SWT.BORDER | SWT.V_SCROLL | SWT.MULTI);
			preview.setEditable(false);
			preview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			sashForm.setWeights(new int[] { 60, 40 });
		}
		createActions();
		createMenus();
		createToolbar();
		createContextMenu();
		hookListeners();

		treeViewer.setInput(getRootInput());
	}

	/**
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static Object[] appendArrays(Object[] array1, Object[] array2) {
		Object[] newArray = new Object[array1.length + array2.length];
		System.arraycopy(array1, 0, newArray, 0, array1.length);
		System.arraycopy(array2, 0, newArray, array1.length, array2.length);
		return newArray;
	}

	/**
	 * Adding event listeners
	 */
	protected void hookListeners() {
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				// if the selection is empty clear the label
				if (event.getSelection().isEmpty()) {
					preview.setText("");
					return;
				}

				if (event.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					StringBuffer toPreview = new StringBuffer("");

					Object element = selection.getFirstElement();
					preview.setText("");

					if (element instanceof File) {
						File selectedfile = (File) selection.getFirstElement();

						if (selectedfile.isDirectory()) {
							return;
						}

						// get the full path to the file
						String file = selectedfile.getAbsolutePath();

						try {

							templateReader.read(file);
							toPreview.append(templateReader.getTemplateDescription());
							toPreview.append("\n\n");
							toPreview.append(templateReader.getTemplateCodeBlock());

						} catch (Exception e) {
							FBLog.logError(Activator.PLUGIN_ID, e);
						}

						preview.setText(toPreview.toString());
					}
				}
			}
		});

		treeViewer.addDoubleClickListener(new TemplatesViewDoubleClickListener(this));
	}

	/**
	 * creates all the default actions
	 */
	protected void createActions() {
		refreshViewAction = new Action("Refresh view", IconResources.getImageRegistry().getDescriptor(IconResources.VIEW_REFRESH)) {
			@Override
			public void run() {
				refreshView();
			}
		};
		refreshViewAction.setToolTipText("Refresh view");

		insertAction = new Action("Insert", IconResources.getImageRegistry().getDescriptor(IconResources.TEMPLATE_INSERT)) {
			@Override
			public void run() {
				insertTemplate();
			}
		};
		insertAction.setToolTipText("Insert the selected snippet into the editor");

		createFolderAction = new Action("Create Folder", IconResources.getImageRegistry().getDescriptor(IconResources.FOLDER_ADD)) {
			@Override
			public void run() {
				createTemplateFolder();
			}
		};
		createFolderAction.setToolTipText("Create a new folder");

		createTemplateAction = new Action("Create New Snippet", IconResources.getImageRegistry().getDescriptor(IconResources.TEMPLATE_ADD)) {
			@Override
			public void run() {
				createTemplate();
			}
		};
		createTemplateAction.setToolTipText("Create a new snippet");

		editTemplateAction = new Action("Edit Snippet", IconResources.getImageRegistry().getDescriptor(IconResources.TEMPLATE_EDIT)) {
			@Override
			public void run() {
				editTemplate();
			}
		};
		editTemplateAction.setToolTipText("Edit the selected snippet");

		deleteTemplateAction = new Action("Delete snippet", IconResources.getImageRegistry().getDescriptor(IconResources.TEMPLATE_DELETE)) {
			@Override
			public void run() {
				deleteTemplate();
			}
		};
		deleteTemplateAction.setToolTipText("Delete selected snippet");

		deleteFolderAction = new Action("Delete Folder", IconResources.getImageRegistry().getDescriptor(IconResources.FOLDER_DELETE)) {
			@Override
			public void run() {
				deleteTemplateFolder();
			}
		};
		deleteFolderAction.setToolTipText("Delete selected folder (must be empty)");
	}

	/**
	 * Creating menues
	 */
	protected void createMenus() {
		IMenuManager rootMenuManager = getViewSite().getActionBars().getMenuManager();

		rootMenuManager.add(refreshViewAction);
		rootMenuManager.add(insertAction);
		rootMenuManager.add(createTemplateAction);
		rootMenuManager.add(editTemplateAction);
		rootMenuManager.add(deleteTemplateAction);
		rootMenuManager.add(createFolderAction);
		rootMenuManager.add(deleteFolderAction);
	}

	/**
	 * Create context menu.
	 */
	private void createContextMenu() {
		menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});

		Menu menu = menuMgr.createContextMenu(treeViewer.getControl());
		treeViewer.getControl().setMenu(menu);

		getSite().registerContextMenu(menuMgr, treeViewer);
	}

	/**
	 * Filling context menu.
	 * 
	 * @param mgr
	 * @see IMenuManager
	 */
	private void fillContextMenu(IMenuManager mgr) {
		File selectedFile = getSelectedFile();

		if (selectedFile.isDirectory()) {
			mgr.add(refreshViewAction);
			mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			mgr.add(createFolderAction);
			mgr.add(createTemplateAction);
			String[] files = selectedFile.list();
			if (files.length == 0) {
				mgr.add(deleteFolderAction);
			}

		} else {
			mgr.add(refreshViewAction);
			mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
			mgr.add(insertAction);
			mgr.add(editTemplateAction);
			mgr.add(deleteTemplateAction);
		}
	}

	/**
	 * Creating the toolbar
	 */
	protected void createToolbar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
		toolbarManager.add(refreshViewAction);
		toolbarManager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		toolbarManager.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		toolbarManager.add(insertAction);
		toolbarManager.add(createTemplateAction);
		toolbarManager.add(editTemplateAction);
		toolbarManager.add(deleteTemplateAction);
		toolbarManager.add(createFolderAction);
		toolbarManager.add(deleteFolderAction);

	}

	/**
	 * Gets the root directory used as the template base
	 * 
	 * @return the root directory
	 */
	public File getRootInput() {
		return codeTemplatesPath.toFile();
	}

	/**
	 * Gets the selected item parses it, and adds the defined stuff to the
	 * editor
	 */
	public void insertTemplate() {
		IEditorPart ieditorpart = this.getViewSite().getWorkbenchWindow().getActivePage().getActiveEditor();
		tmpAction.setActiveEditor(null, ieditorpart);

		Object element = null;
		String codeBlock = null;

		if (treeViewer.getSelection().isEmpty()) {
			return;
		} else {
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			element = selection.getFirstElement();
		}

		if (element instanceof File) {
			File selectedfile = (File) element;
			if (selectedfile.isDirectory()) {
				return;
			}

			templateReader.read(selectedfile.getAbsolutePath());

			try {
				IFile activeFile = null;
				if (ieditorpart.getEditorInput() instanceof IFileEditorInput) {
					// This section should be triggered when focus is in the
					// file editor/package explorer
					activeFile = ((IFileEditorInput) ieditorpart.getEditorInput()).getFile();
				}

				codeBlock = TemplateVarParser.parse(templateReader.getTemplateCodeBlock(), activeFile, this.getViewSite().getShell(), false);
			} catch (Exception exception) {
				FBLog.logError(Activator.PLUGIN_ID, exception);
			}
		}
		if (codeBlock != null) {
			tmpAction.setEnclosingStrings(codeBlock, "");
			tmpAction.run(null);
			ieditorpart.setFocus();
		}
	}

	/**
	 * Returns the currently selected file or the root directory if nothing is
	 * selected
	 */
	private File getSelectedFile() {
		File selectedfile = null;

		if (treeViewer.getSelection().isEmpty()) {
			selectedfile = getRootInput();
		} else {
			IStructuredSelection selection = (IStructuredSelection) treeViewer.getSelection();
			selectedfile = (File) selection.getFirstElement();
			treeViewer.setExpandedState(selection.getFirstElement(), true);

		}
		return selectedfile;
	}

	/**
	 * Creates a new folder called below the currently active folder If no
	 * folder is currently active it creates the folder below the root.
	 * 
	 */
	protected void createTemplateFolder() {
		File selectedfile = getSelectedFile();

		if (!selectedfile.isDirectory()) {
			selectedfile = selectedfile.getParentFile();
		}

		TemplateWriter writer = new TemplateWriter(selectedfile, templateType, codeTemplatesPath);
		NewFolderDialog folderDialog = new NewFolderDialog(this.getViewSite().getShell(), writer, this.treeViewer);
		folderDialog.open();

	}

	/**
	 * Deletes a template folder. Folder must be empty before it is being
	 * deleted.
	 */
	protected void deleteTemplateFolder() {
		File selectedfile = getSelectedFile();

		if (!selectedfile.isDirectory()) {
			selectedfile = selectedfile.getParentFile();
		}

		if (selectedfile.listFiles().length > 0) {
			MsgDialogs.Warning("You must delete all the code templates in this folder first");
		} else {
			final boolean retVal = MessageDialog.openQuestion(PlatformUtils.getShell(), "Delete Folder", "Are you sure you want to delete this folder?");
			if (retVal) {
				selectedfile.delete();
				refreshView();
			}
		}

	}

	/**
	 * Creating a new code template
	 */
	protected void createTemplate() {
		File selectedfile = getSelectedFile();

		if (!selectedfile.isDirectory()) {
			selectedfile = selectedfile.getParentFile();
		}
		templateType = TEMPLATE_TYPE;
		TemplateWriter writer = new TemplateWriter(selectedfile, templateType, codeTemplatesPath);
		TemplateFileDialog newTemplateFileDialog = new TemplateFileDialog(this.getViewSite().getShell(), writer, this.treeViewer, "", "", "", "");
		newTemplateFileDialog.open();
	}

	/**
	 * Deleting selected template
	 */
	protected void deleteTemplate() {
		File selectedfile = getSelectedFile();

		if (selectedfile.isDirectory()) {
			MsgDialogs.Warning("Use Delete Folder when deleting a folder !");
			return;
		}
		MessageBox deleteDialog = new MessageBox(this.getViewSite().getShell(), SWT.YES | SWT.NO);
		deleteDialog.setMessage("Are you sure you want to delete this template ?");
		if (deleteDialog.open() == SWT.YES) {
			selectedfile.delete();
			// XXX Change so properties file is being updated correctly here.
			// currently the entry in the file is not deleted
			refreshView();
		}

	}

	/**
	 * Editing Code template
	 */
	protected void editTemplate() {
		File selectedfile = getSelectedFile();

		if (selectedfile.isDirectory()) {
			return;
		}

		File parentDirectory = selectedfile.getParentFile();

		String selectedFile = selectedfile.getAbsolutePath().toLowerCase();
		templateType = TEMPLATE_TYPE;

		templateReader.read(selectedFile);

		PropertyFileReader propertyFileReader = new PropertyFileReader();

		String templateName = selectedfile.getName().substring(0, selectedfile.getName().length() - 4);

		String templateSeq = getTemplateSequence(selectedfile);

		String templateTrigger = propertyFileReader.getSequence(templateSeq);
		String templateDescription = templateReader.getTemplateDescription();
		String templateCodeBlock = templateReader.getTemplateCodeBlock();
		TemplateWriter writer = new TemplateWriter(parentDirectory, templateType, codeTemplatesPath);
		TemplateFileDialog newTemplateFileDialog = new TemplateFileDialog(this.getViewSite().getShell(), writer, this.treeViewer, templateName,
				templateTrigger, templateDescription, templateCodeBlock);
		newTemplateFileDialog.open();

	}

	/**
	 * Computing new relative path for template
	 * 
	 * @param selectedFile
	 *            File Selected file in TreeView
	 * @return String Value of template name and path relative to the directory
	 *         stored in preferences.
	 */
	private String getTemplateSequence(File selectedFile) {
		String filepath = selectedFile.getAbsolutePath().replaceAll("\\\\", "/");
		String basePath = PreferenceUtils.getTemplatePath().replaceAll("\\\\", "/");
		String relativePath = filepath.replaceFirst(basePath, "");
		return relativePath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse
	 * .jface.util.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent event) {

		if (event.getProperty().equals(PreferenceConstants.P_TEMPLATES_PATH)) {
			codeTemplatesPath = new Path(event.getNewValue().toString());
			treeViewer.setInput(getRootInput());
		}
	}

	/*
	 * (non-Javadoc) Clients should not call this method but it must be
	 * implemented
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
	}

	/**
	 * Refreshing templatesview
	 */
	protected void refreshView() {
		codeTemplatesPath = new Path(PreferenceUtils.getTemplatePath());
		treeViewer.setInput(getRootInput());
		treeViewer.refresh(true);
	}
}
