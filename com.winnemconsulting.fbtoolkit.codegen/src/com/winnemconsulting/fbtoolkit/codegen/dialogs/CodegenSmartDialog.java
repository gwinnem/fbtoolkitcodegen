/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.dialogs;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.model.vo.TemplateVarItemVO;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Helper class for {@link CodegenDialog}</br> Parsing text used in dialog
 * 
 * @author Geirr Winnem
 */
public final class CodegenSmartDialog {

	/**
	 * Dialog title text
	 */
	private static final String DIALOG_TITLE = "User input needed";
	/**
	 * Instance of this class
	 */
	private static CodegenSmartDialog instance = null;

	/**
	 * Constructor
	 */
	private CodegenSmartDialog() {
		super();
	}

	/**
	 * @return this instance
	 */
	public static CodegenSmartDialog getInstance() {
		if (instance == null) {
			instance = new CodegenSmartDialog();
		}
		return instance;
	}

	/**
	 * Parsing code template
	 * 
	 * @param strToParse
	 *            String to parse
	 * @param shell
	 *            {@link Shell}
	 * @return Parsed string
	 */
	public static String parse(final String strToParse, final Shell shell) {
		String newStr = strToParse;
		final ArrayList<TemplateVarItemVO> list = new ArrayList<TemplateVarItemVO>();
		int position = 0;

		while (newStr.indexOf("$${", position) >= 0) {
			final int expressionStart = newStr.indexOf("$${", position) + 3;
			final int expressionEnd = newStr.indexOf("}", expressionStart);
			final String expression = newStr.substring(expressionStart,
					expressionEnd);
			
			//Checking if we have a default value
			final String stringArray[] = expression.split(":");
			final String variable = stringArray[0];
			String defaultValue = "";
			if (stringArray.length > 1) {
				defaultValue = stringArray[1];
			}
			
			//Should be combobox
			final String optionValArray[] = defaultValue.split("\\|");

			TemplateVarItemVO item = new TemplateVarItemVO(variable,
					optionValArray, expression);

			Iterator<TemplateVarItemVO> i = list.iterator();

			boolean duplicateItem = false;
			while (i.hasNext()) {
				if (i.next().getOriginal().equalsIgnoreCase(expression)) {
					duplicateItem = true;
				}
			}

			if (!duplicateItem) {
				list.add(item);
			}

			position = expressionEnd;
		}

		if (list.iterator().hasNext()) {

			CodegenDialog codegenDialog = new CodegenDialog(shell);
			codegenDialog.setItemList(list);
			codegenDialog.setTitle(DIALOG_TITLE);
			if (codegenDialog.open() == Dialog.OK) {
				try {
					Iterator<TemplateVarItemVO> i = list.iterator();
					while (i.hasNext()) {
						TemplateVarItemVO item = i.next();
						String original = "$${" + item.getOriginal() + "}";
						String replacement = item.getReplacement();
						newStr = doReplacement(newStr, original, replacement);
					}
				} catch (Exception exception) {
					FBLog.logError(Activator.PLUGIN_ID, exception);
				}

				codegenDialog.close();
			} else {
				return null;
			}

		}

		return newStr;
	}

	/**
	 * String replacement helper function
	 * 
	 * @param oldStr
	 *            Original string to replace strings within
	 * @param original
	 *            String to replace
	 * @param replacement
	 *            String replacement value
	 * @return New string with replacements
	 */
	private static String doReplacement(final String oldStr,
			final String original, final String replacement) {
		StringBuffer buffer = new StringBuffer(oldStr);
		int fromOffset = 0;
		while (true) {
			fromOffset = buffer.indexOf(original, fromOffset);
			if (fromOffset >= 0) {
				buffer.replace(fromOffset, fromOffset + original.length(),
						replacement);
				fromOffset = fromOffset + replacement.length();
			} else {
				break;
			}
		}

		return buffer.toString();
	}
}
