/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.dialogs;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.winnemconsulting.fbtoolkit.codegen.dialogs.listeners.CodeDialogsComboModifyListener;
import com.winnemconsulting.fbtoolkit.codegen.model.vo.TemplateVarItemVO;
import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;

/**
 * Dialog for handling code replacement when generating code
 * 
 * @author Geirr Winnem
 */
public class CodegenDialog extends Dialog {
	protected String title;

	private transient ArrayList<?> itemList;

	private Combo combo;
	private Text text;
	private ComboViewer comboViewer;
	private Label label;
	private GridData gridData;

	/**
	 * Constructor for this class
	 */
	public CodegenDialog(final Shell parentShell) {
		super(parentShell);
		super.setDefaultImage(IconResources.get(IconResources.DIALOG_INPUT));
	}

	/**
	 * Actually creates the dialog area
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		final Composite container = (Composite) super.createDialogArea(parent);
		final GridLayout gridLayoyt = new GridLayout();
		gridLayoyt.numColumns = 2;

		container.setLayout(gridLayoyt);
		//final FontData labelFontData = new FontData();
		//labelFontData.setStyle(SWT.BOLD);
		//final Font labelFont = new Font(parent.getDisplay(), labelFontData);

		Iterator<?> i = itemList.iterator();
		TemplateVarItemVO item;
		while (i.hasNext()) {
			item = (TemplateVarItemVO) i.next();

			label = new Label(container, SWT.HORIZONTAL);
			label.setText(item.getName() + ":");
			//label.setFont(labelFont);

			gridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL);

			gridData.widthHint = 200;
			if (item.getValues().length > 1) {
				addComboField(container, item, gridData);
			} else {
				addTextField(container, item, gridData);
			}
		}

		return container;
	}

	/**
	 * Adding ComboFields to dialog
	 * 
	 * @param parent
	 * @param item
	 * @param gridData
	 */
	private void addComboField(Composite parent, TemplateVarItemVO item,
			GridData gridData) {

		String[] items = item.getValues();

		setCombo(new Combo(parent, SWT.DROP_DOWN));
		getCombo().setLayoutData(gridData);

		CodeDialogsComboModifyListener modifyListener = new CodeDialogsComboModifyListener(
				getCombo());

		getCombo().addModifyListener(modifyListener);
		getCombo().addKeyListener(modifyListener);
		item.setCombo(getCombo());
		getCombo().setItems(items);
		getCombo().select(0);
	}

	/**
	 * Adding text fields to dialog
	 * 
	 * @param parent
	 * @param item
	 * @param gridData
	 */
	private void addTextField(Composite parent, TemplateVarItemVO item,
			GridData gridData) {

		String[] items = item.getValues();

		final Text text = new Text(parent, SWT.BORDER);

		text.setLayoutData(gridData);

		item.setText(text);
		text.setText(items[0]);

//		final Button button = new Button(parent, SWT.NONE | SWT.MULTI);
//		button.setText(">");
//		button.addMouseListener(new MouseListener() {
//
//			/*
//			 * (non-Javadoc)
//			 * 
//			 * @see
//			 * org.eclipse.swt.events.MouseListener#mouseDown(org.eclipse.swt
//			 * .events.MouseEvent)
//			 */
//			public void mouseDown(MouseEvent event) {
//
//				if (button.getText().equals(">")) {
//					text.setSize(text.getSize().x, text.getSize().y + 40);
//					button.setText("V");
//				} else {
//					text.setSize(text.getSize().x, text.getSize().y - 40);
//					button.setText(">");
//				}
//
//			}
//
//			/*
//			 * (non-Javadoc)
//			 * 
//			 * @see
//			 * org.eclipse.swt.events.MouseListener#mouseDoubleClick(org.eclipse
//			 * .swt.events.MouseEvent)
//			 */
//			public void mouseDoubleClick(MouseEvent event) {// NONPMD
//			}
//
//			/*
//			 * (non-Javadoc)
//			 * 
//			 * @see
//			 * org.eclipse.swt.events.MouseListener#mouseUp(org.eclipse.swt.
//			 * events.MouseEvent)
//			 */
//			public void mouseUp(MouseEvent event) {// NONPMD
//			}
//
//		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(this.title);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			Iterator<?> i = itemList.iterator();

			while (i.hasNext()) {
				((TemplateVarItemVO) i.next()).setReplacement();
			}
		}
		super.buttonPressed(buttonId);

	}

	/**
	 * @param itemList
	 */
	public void setItemList(ArrayList<?> itemList) {
		this.itemList = itemList;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param combo
	 *            the combo to set
	 */
	private void setCombo(Combo combo) {
		this.combo = combo;
	}

	/**
	 * @return the combo
	 */
	private Combo getCombo() {
		return combo;
	}

	/**
	 * @param comboViewer
	 *            the comboViewer to set
	 */
	public void setComboViewer(ComboViewer comboViewer) {
		this.comboViewer = comboViewer;
	}

	/**
	 * @return the comboViewer
	 */
	public ComboViewer getComboViewer() {
		return comboViewer;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(Text text) {
		this.text = text;
	}

	/**
	 * @return the text
	 */
	public Text getText() {
		return text;
	}
}
