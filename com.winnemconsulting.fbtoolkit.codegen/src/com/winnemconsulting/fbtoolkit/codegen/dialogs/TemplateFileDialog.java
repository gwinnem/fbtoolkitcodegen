/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateWriter;

/**
 * Dialog for creating a new code snippet or editing existing
 * 
 * @author Geirr Winnem
 */
public class TemplateFileDialog extends Dialog {

	/**
	 * Dialog title
	 */
	private static final String DIALOG_TITLE = "Code Snippet Editor";

	/**
	 * Label text for Name
	 */
	private static final String LABEL_TPL_NAME = "Snippet name:";

	/**
	 * Label text for Trigger
	 */
	private static final String LABEL_TPL_TRIGGER = "Trigger text:";

	/**
	 * Label text for description
	 */
	private static final String LABEL_TPL_DESCR = "Short description:";

	/**
	 * Label for template code block
	 */
	private static final String LABEL_TPL_CODE = "Code Snippet:";

	/**
	 * TreeViewer instance
	 */
	private final TreeViewer treeView;

	/**
	 * Name Text field
	 */
	private Text txtName;

	/**
	 * Trigger Text field
	 */
	private Text templateTriggerText;

	/**
	 * Description Text field
	 */
	private Text templateDescriptionText;

	/**
	 * Template code Text field
	 */
	private Text templateCodeText;

	/**
	 * Original name of template
	 */
	private String templateOrgName = "";
	private String templateNameValue = "";
	private String templateTriggerValue = "";
	private String templateDescriptionValue = "";
	private String templateCodeValue = "";

	/**
	 * Position of cursor in the code text field
	 */
	private int cursorPosition = 0;

	/**
	 * Class writing actual template file
	 */
	private final TemplateWriter writer;

	/**
	 * Ok button widget.
	 */
	private Button okButton;

	/**
	 * Error message label widget.
	 */
	private Label errorMessageLabel;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            {@link Shell}
	 * @param fileWriter
	 *            {@link TemplateWriter} to use
	 * @param treeView
	 *            {@link TreeViewer} TemplatesView instance
	 * @param templateNameInitialValue
	 *            Name of template.
	 * @param templateTrigger
	 *            Trigger text.
	 * @param templateDescriptionInitialValue
	 *            Template Description.
	 * @param templateCodeInitialValue
	 *            Template code Text.
	 */
	public TemplateFileDialog(final Shell parent,
			final TemplateWriter fileWriter, TreeViewer treeView,
			final String templateNameInitialValue,
			final String templateTrigger,
			final String templateDescriptionInitialValue,
			final String templateCodeInitialValue) {
		super(parent);
		setShellStyle(SWT.DIALOG_TRIM | SWT.MAX | SWT.RESIZE);
		super.setDefaultImage(IconResources.get(IconResources.TEMPLATE_EDIT));

		if (templateNameInitialValue != null) {
			templateNameValue = templateNameInitialValue;
			templateOrgName = templateNameInitialValue;
		}

		if (templateTrigger != null) {
			templateTriggerValue = templateTrigger;
		}

		if (templateDescriptionInitialValue != null) {
			templateDescriptionValue = templateDescriptionInitialValue;
		}

		if (templateCodeInitialValue != null) {
			templateCodeValue = templateCodeInitialValue;
		}

		this.treeView = treeView;
		writer = fileWriter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets
	 * .Composite)
	 */
	@Override
	protected Control createDialogArea(final Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout gridLayout = (GridLayout) container.getLayout();
		gridLayout.numColumns = 2;
		gridLayout.horizontalSpacing = 1;
		gridLayout.verticalSpacing = 5;
		gridLayout.marginWidth = 10;
		gridLayout.marginHeight = 10;

		Label lblName = new Label(container, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false,
				1, 1));
		lblName.setText(LABEL_TPL_NAME);
		txtName = new Text(container, SWT.BORDER);
		txtName.addModifyListener(new ModifyListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.
			 * swt.events.ModifyEvent)
			 */
			public void modifyText(ModifyEvent e) {
				validateInput();
			}
		});
		txtName.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1,
				1));

		Label lblTrigger = new Label(container, SWT.NONE);
		lblTrigger.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblTrigger.setText(LABEL_TPL_TRIGGER);

		templateTriggerText = new Text(container, SWT.BORDER);
		templateTriggerText.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				false, false, 1, 1));
		templateTriggerText.addModifyListener(new ModifyListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.
			 * swt.events.ModifyEvent)
			 */
			public void modifyText(final ModifyEvent event) {
				validateInput();
			}
		});

		Label lblDescr = new Label(container, SWT.NONE);
		lblDescr.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
				false, 1, 1));
		lblDescr.setText(LABEL_TPL_DESCR);

		templateDescriptionText = new Text(container, SWT.BORDER);
		templateDescriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL,
				false, false, 1, 1));
		templateDescriptionText.addModifyListener(new ModifyListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.
			 * swt.events.ModifyEvent)
			 */
			public void modifyText(final ModifyEvent event) {
				validateInput();
			}
		});
		Label lblPattern = new Label(container, SWT.NONE);
		lblPattern.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false,
				1, 1));
		lblPattern.setText(LABEL_TPL_CODE);

		Label lblEmpty = new Label(container, SWT.NONE);
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1));

		templateCodeText = new Text(container, SWT.BORDER | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.MULTI);
		templateCodeText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 2, 1));

		Button btnVariables = new Button(container, SWT.NONE);
		btnVariables.setText("variables");
		btnVariables.addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse
			 * .swt.events.SelectionEvent)
			 */
			public void widgetSelected(SelectionEvent e) {
				// Storing cursor position so we can insert template variables
				// later
				cursorPosition = templateCodeText.getCaretPosition();

				TemplateVariablesDialog tp = new TemplateVariablesDialog(
						getShell());
				int intRet = tp.open();
				if (intRet == Dialog.OK) {
					// Insert value from tp
					String tmpInsert = templateCodeText.getText();
					if (cursorPosition == 0) {
						templateCodeText.setText(tp.getTemplateVariable()
								+ tmpInsert);
					} else {
						tmpInsert = tmpInsert.substring(0, cursorPosition)
								+ tp.getTemplateVariable()
								+ tmpInsert.substring(cursorPosition);
						templateCodeText.setText(tmpInsert);
					}
					templateCodeText.setSelection(cursorPosition
							+ tp.getTemplateVariable().length());
					templateCodeText.setFocus();
				}

			}
		});
		lblEmpty = new Label(container, SWT.NONE);
		lblEmpty.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false,
				1, 1));
		errorMessageLabel = new Label(container, SWT.WRAP);
		errorMessageLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9,
				SWT.BOLD));
		errorMessageLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false,
				false, 2, 1));
		final Color color = new Color(Display.getCurrent(), 255, 0, 0);
		errorMessageLabel.setForeground(color);
		
		return container;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		// Used to determine if name is changed in template
		Boolean isNameDirty = false;

		if (!templateOrgName.equals("")) {
			if (!templateOrgName.equals(txtName.getText())) {
				isNameDirty = true;
			}
		}
		writer.writeTemplate(txtName.getText(), templateTriggerText.getText(),
				templateDescriptionText.getText(), templateCodeText.getText(),
				isNameDirty, templateOrgName);

		close();
		treeView.refresh();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(final int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			templateNameValue = txtName.getText();
			templateTriggerValue = templateTriggerText.getText();
			templateDescriptionValue = templateDescriptionText.getText();
			templateCodeValue = templateCodeText.getText();
		} else {
			templateNameValue = null;
			templateTriggerValue = null;
			templateDescriptionValue = null;
			templateCodeValue = null;
			templateOrgName = null;
		}
		super.buttonPressed(buttonId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		shell.setText(DIALOG_TITLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(final Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID,
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
		txtName.setFocus();
		if (templateNameValue != null) {
			txtName.setText(templateNameValue);
			txtName.selectAll();
		}

		if (templateTriggerValue != null) {
			templateTriggerText.setText(templateTriggerValue);
		}

		if (templateDescriptionValue != null) {
			templateDescriptionText.setText(templateDescriptionValue);
		}
		if (templateCodeValue != null) {
			templateCodeText.setText(templateCodeValue);
		}
	}

	/**
	 * @return errorMessageLabel
	 */
	protected Label getErrorMessageLabel() {
		return errorMessageLabel;
	}

	/**
	 * @return okButton
	 */
	protected Button getOkButton() {
		return okButton;
	}

	/**
	 * Event handler for validating input
	 */
	protected void validateInput() {

		String errorMessage = null;
		if (!txtName.getText().matches("[0-9a-zA-Z _-]+")) {
			errorMessage = "The template name can only contain numbers, alphabetic characters, space underscore and dash.";
		} else if (templateTriggerText.getText().length() > 0
				&& !templateTriggerText.getText().matches("[0-9a-zA-Z_-]+")) {
			errorMessage = "The trigger text can only contain numbers, alphabetic characters, underscore and dash.";
		} else if (templateDescriptionText.getText().length() == 0) {
			errorMessage = "Description is missing";
		}
		errorMessageLabel.setText(errorMessage == null ? "" : errorMessage); //$NON-NLS-1$

		okButton.setEnabled(errorMessage == null);

		errorMessageLabel.getParent().update();
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(600, 450);
	}
}
