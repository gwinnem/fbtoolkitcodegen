/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.dialogs;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;

import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateWriter;

/**
 * Simple folder dialog allowing user to create a new code template folder
 * 
 * @author Geirr Winnem
 * 
 */
public class NewFolderDialog extends InputDialog {
	private final TreeViewer treeView;
	private static String dialogTitle = "New Folder";
	private static String dialogMessage = "New Folder Name: ";
	private static String initialValue = "";
	private final TemplateWriter writer;

	/**
	 * Constructor
	 * 
	 * @param parent
	 *            {@link Shell}
	 * @param filewriter
	 *            {@link TemplateWriter}
	 * @param treeView
	 *            {@link Tree}
	 */
	public NewFolderDialog(final Shell parent, final TemplateWriter filewriter,
			final TreeViewer treeView) {
		super(parent, dialogTitle, dialogMessage, initialValue, null);
		this.writer = filewriter;
		this.treeView = treeView;
		super.setDefaultImage(IconResources.get(IconResources.FOLDER_ADD));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		writer.writeFolder(this.getValue());
		close();
		treeView.refresh();

	}
}
