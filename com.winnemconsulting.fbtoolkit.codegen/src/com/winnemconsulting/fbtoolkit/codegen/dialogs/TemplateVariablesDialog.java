package com.winnemconsulting.fbtoolkit.codegen.dialogs;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.winnemconsulting.fbtoolkit.codegen.utils.IconResources;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateVarParser;

public class TemplateVariablesDialog extends Dialog {
	private Text varDescription;
	private List varList;

	/**
	 * Container for template variables and the description
	 */
	private Hashtable<String, String> templateVariables = new Hashtable<String, String>();

	/**
	 * Container storing selected template variable
	 */
	private String templateVariable = "";

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public TemplateVariablesDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		setDefaultImage(IconResources.get(IconResources.DIALOG_INPUT));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets
	 * .Shell)
	 */
	@Override
	protected void configureShell(final Shell shell) {
		super.configureShell(shell);
		shell.setText("Code Snippet variables");
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblVariables = new Label(container, SWT.NONE);
		lblVariables.setText("Variables");

		Label lblDescription = new Label(container, SWT.NONE);
		lblDescription.setText("Description");

		varList = new List(container, SWT.BORDER | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_list.heightHint = 162;
		gd_list.widthHint = 174;
		varList.setLayoutData(gd_list);
		// Populating template variables
		popTemplateVariables();

		populateList();

		varDescription = new Text(container, SWT.BORDER | SWT.READ_ONLY
				| SWT.WRAP | SWT.V_SCROLL);
		varDescription.setEnabled(false);
		varDescription.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true,
				true, 1, 1));

		varList.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
				okPressed();
			}

			public void mouseDown(MouseEvent e) {
				// Do nothing
			}

			public void mouseUp(MouseEvent e) {
				// Do nothing
			}
		});

		varList.addKeyListener(new KeyListener() {

			public void keyPressed(KeyEvent event) {
				if (event.keyCode == SWT.KEYPAD_CR || event.keyCode == SWT.CR) {
					okPressed();
				}
			}

			public void keyReleased(KeyEvent event) {
				// Do nothing
			}
		});

		varList.addSelectionListener(new SelectionListener() {

			public void widgetDefaultSelected(SelectionEvent e) {
				// Do nothing

			}

			public void widgetSelected(SelectionEvent e) {
				StringBuilder sb = new StringBuilder();
				String tmp = varList.getItem(varList.getSelectionIndex());
				sb.append("Keyword:\n");
				sb.append(genKeyword(tmp));
				sb.append("\n\n");
				sb.append("Description:\n");
				sb.append(getVarDescription(tmp));

				varDescription.setText(sb.toString());

				templateVariable = genKeyword(tmp);

			}
		});

		return container;
	}

	/**
	 * Generating template variable
	 * 
	 * @param keyword
	 *            variable to generate
	 * @return template variable
	 * @throws IllegalArgumentException
	 */
	private String genKeyword(final String keyword) {
		if (keyword.length() == 0 || keyword == null) {
			throw new IllegalArgumentException(
					"Empty template variable keyword not allowed");
		}
		return "${" + keyword + "}";
	}

	/**
	 * Generating example output text for a date variable
	 * 
	 * @param keyword
	 *            variable to generate example from
	 * @return example variable output
	 */
	private String getExampleDate(final String keyword) {
		return "Example output:\n"
				+ TemplateVarParser.parseDateVariables("${" + keyword + "}");
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	/**
	 * @return the templateVariable
	 */
	public String getTemplateVariable() {
		return templateVariable;
	}

	/**
	 * Getting the template variable description
	 * 
	 * @param keyword
	 *            templatevariable
	 * @return description for the templatevariable.
	 */
	private String getVarDescription(final String keyword) {
		String retVal;
		try {
			retVal = templateVariables.get(keyword);
		} catch (NullPointerException e) {
			retVal = "";
		}
		return retVal;
	}

	/**
	 * Populating hashtable containing templatevariable and description
	 */
	private void popTemplateVariables() {
		templateVariables.put("full_date", "Full date including timezone.\n\n"
				+ getExampleDate("full_date"));

		templateVariables.put("current_date", "Current date.\n\n"
				+ getExampleDate("current_date"));

		templateVariables.put("current_month", "Current month.\n\n"
				+ getExampleDate("current_month"));

		templateVariables.put("current_time", "Current time.\n\n"
				+ getExampleDate("current_time"));

		templateVariables.put("date_time", "Date time.\n\n"
				+ getExampleDate("date_time"));

		templateVariables
				.put(
						"day_of_week",
						"Day of week (the week starts on Sunday).\nOutput is dependent on system locale.\n\n"
								+ getExampleDate("day_of_week"));

		templateVariables.put("month_number",
				"Month as a numeric value from 1 to 12.");

		templateVariables.put("day_of_month",
				"Day of the month as a numeric value.");

		templateVariables.put("day_of_week_number",
				"Day of week (the week starts on Sunday) as a numeric value.");

		templateVariables.put("date_time24",
				"A 24 hour clock version of date_time.\n\n"
						+ getExampleDate("date_time24"));

		templateVariables.put("current_year", "Current year in 4 digits.");

		templateVariables
				.put("current_short_year", "Current year in 2 digits.");

		templateVariables.put("class_name",
				"Name of the class.\nUse this only in class files.");

		templateVariables.put("current_file", "The filename of the resource.");

		templateVariables.put("current_folder",
				"Folder name where class is located.");

		templateVariables.put("current_full_path",
				"Full path including filename.");

		templateVariables.put("current_project_path", "Project folder.");

		templateVariables.put("user_name",
				"User extracted from system environment variables.");

		templateVariables.put("current_project_name", "The project name.");

		templateVariables.put("current_project_root",
				"The project root folder on disk.");

		templateVariables
				.put("package_definition",
						"Namespace class file belongs to.\nUse this only in class file templates");
	}

	/**
	 * Populating list widget
	 * 
	 */
	private void populateList() {
		Enumeration<String> varEnum = templateVariables.keys();
		while (varEnum.hasMoreElements()) {
			varList.add(varEnum.nextElement());
		}
	}
}
