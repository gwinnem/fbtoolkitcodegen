/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.dialogs.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Combo;

/**
 * ModifyListener for Comboboxes in CodegenDialog
 * 
 * @author Geirr Winnem
 */
public class CodeDialogsComboModifyListener implements ModifyListener,
		KeyListener {

	private transient final Combo combo;
	private transient int lastKey = -1;

	public CodeDialogsComboModifyListener(final Combo combo) {
		this.combo = combo;
	}

	public void modifyText(final ModifyEvent event) {
		try {

			if (this.lastKey == SWT.DEL || this.lastKey == SWT.BS
					|| this.lastKey == -1) {
				return;
			}

			final String[] items = combo.getItems();
			final int getSelectionx = combo.getSelection().x;
			final int getSizey = combo.getSize().y;

			for (int i = 0; i < items.length; i++) {
				if (items[i].startsWith(combo.getText())) {
					this.lastKey = -1;
					combo.select(i);
					combo.setSelection(new Point(getSelectionx, getSizey));

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void keyPressed(KeyEvent event) {
		this.lastKey = event.keyCode;
	}

	public void keyReleased(KeyEvent event) {
		this.lastKey = -1;
	}

}
