/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.model.vo.TemplatePartVO;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceUtils;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Class reading code templates
 * 
 * @author Geirr Winnem
 */
public class TemplateReader {

	protected DocumentBuilder documentBuilder;
	protected DocumentBuilderFactory documentFactory;

	/**
	 * Xml document to parse
	 */
	private Document document = null;

	/**
	 * Description of template
	 */
	private String templateDescription;
	/**
	 * Code from template file
	 */
	private String templateCodeBlock;

	/**
	 * Template File
	 */
	private File templateFile;

	/**
	 * Constructor
	 */
	public TemplateReader() {
		super();

		try {
			documentFactory = javax.xml.parsers.DocumentBuilderFactory
					.newInstance();
			documentFactory.setIgnoringComments(true);
			documentFactory.setIgnoringElementContentWhitespace(true);
			documentFactory.setCoalescing(true);
			documentBuilder = documentFactory.newDocumentBuilder();
		} catch (ParserConfigurationException exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
		}
	}

	/**
	 * Code template reader
	 * 
	 * @param file
	 *            File Object
	 * @see File
	 */
	public void read(final File file) {
		read(file.getAbsoluteFile());
	}

	/**
	 * Code template reader
	 * 
	 * @param fileName
	 *            String name of file
	 */
	public void read(final String fileName) {

		this.templateFile = new File(fileName);
		if (fileName != null) {
		} else {
			this.templateFile = new File("");
		}

		if (!templateFile.exists()) {
			this.templateFile = new File(PreferenceUtils.getTemplatePath()
					+ fileName);
		}

		if (templateFile.exists()) {
			try {
				final FileInputStream fis = new FileInputStream(templateFile);
				final BufferedInputStream bis = new BufferedInputStream(fis);
				bis.mark(0);
				if (((char) bis.read()) != '<') {
					bis.reset();
					@SuppressWarnings("unused")
					int t, p, q;
					t = bis.read();
					p = bis.read();
					q = bis.read();
				} else {
					bis.reset();
				}

				bis.mark(0);
				try {
					document = documentBuilder.parse(bis);
					parseDocument();
				} catch (SAXException exception) {
					FBLog.logError(Activator.PLUGIN_ID, exception);
					this.templateDescription = exception.getMessage();
				}
				bis.close();
			} catch (IOException exception) {
				FBLog.logError(Activator.PLUGIN_ID, exception);
				this.templateDescription = exception.getMessage();
			}

		}

	}

	/**
	 * Parsing XML document
	 */
	private void parseDocument() {
		if (document.equals(null)) {
			return;
		}

		parseTemplateDescription();
		parseTemplateCodeBlock();
	}

	/**
	 *Parsing CodegenTemplate header
	 */
	private void parseTemplateDescription() {
		this.templateDescription = getValue("description", 0);
	}

	/**
	 * Parsing template code block
	 */
	private void parseTemplateCodeBlock() {
		this.templateCodeBlock = getValue("code", 0);
	}

	/**
	 * Getting xml node value from document
	 * 
	 * @param key
	 * @param iteration
	 * @return
	 */
	private String getValue(final String key, final int iteration) {
		int nodetype = 0;

		try {
			nodetype = document.getElementsByTagName(key).item(iteration)
					.getFirstChild().getNodeType();
		} catch (Exception exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
			return "";
		}

		String val = null;

		if (nodetype == Document.TEXT_NODE) {
			val = ((Text) document.getElementsByTagName(key).item(iteration)
					.getFirstChild()).getData();
		} else if (nodetype == Document.CDATA_SECTION_NODE) {
			final CDATASection cds = (CDATASection) document
					.getElementsByTagName(key).item(iteration).getFirstChild();
			val = cds.getNodeValue();
			if (val.endsWith(" ")) {
				val = val.substring(0, val.length() - 1);
			}
		} else {
			val = "";
		}
		return val;
	}

	/**
	 * Creating a new TemplatePartVO
	 * 
	 * @return {@link TemplatePartVO}
	 */
	public TemplatePartVO getTemplate() {

		final TemplatePartVO templatePartVO = new TemplatePartVO();
		templatePartVO.setDescription(getTemplateDescription());
		templatePartVO.setCodeBlock(getTemplateCodeBlock());
		templatePartVO.setName(templateFile.getName());

		return templatePartVO;
	}

	/**
	 * Description node in template file
	 * 
	 * @return String Description
	 */
	public String getTemplateDescription() {
		if (this.templateDescription == null) {
			this.templateDescription = "";
		}
		return this.templateDescription;
	}

	/**
	 * Getting the startCode block
	 * 
	 * @return String
	 */
	public String getTemplateCodeBlock() {
		if (this.templateCodeBlock == null
				|| this.templateCodeBlock.trim().length() == 0) {
			this.templateCodeBlock = "";
		}
		return this.templateCodeBlock;
	}
}
