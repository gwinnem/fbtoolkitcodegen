/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.io.FileOutputStream;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Writing file codetemplates.properties
 * 
 * @author Geirr Winnem
 * 
 */
public class PropertyFileWriter extends AbstractPropertyFile {

	/**
	 * Text used to trigger template
	 * 
	 * @private
	 */
	private String triggerText = "";

	/**
	 * Default Constructor
	 */
	public PropertyFileWriter() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param trigger
	 *            String trigger text.
	 * @param filePath
	 *            String path and filename where code template is located. Path
	 *            is relative to templates path defined in plugin preferences.
	 */
	public PropertyFileWriter(final String trigger, final String filePath) {
		setTriggerText(trigger);
		super.setTemplateFilePath(filePath);
		super.loadProperties();
	}

	@Override
	void execute() {
		final String sequence = super.getSequence(getTemplateFilePath());
		if (sequence == null) {
			super.setCodeTemplateProps(getTriggerText(), getTemplateFilePath());
			// super.clearKeyCombo(sequence);
			writePropertyFile();
		}
	}

	/**
	 * Writing new version of property file.
	 */
	private void writePropertyFile() {
		try {
			final FileOutputStream output = new FileOutputStream(super
					.getCodeTemplatePropsFilePath());
			super.getCodeTemplateProps().store(output, HEADER_TEXT);
		} catch (Exception exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
		}
	}

	/**
	 * @param triggerText
	 *            the triggerText to set
	 */
	@Override
	public void setTriggerText(String triggerText) {
		this.triggerText = triggerText;
	}

	/**
	 * @return the triggerText
	 */
	@Override
	public String getTriggerText() {
		return triggerText;
	}
}
