/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.core.resources.IFile;
import org.eclipse.swt.widgets.Shell;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.dialogs.CodegenSmartDialog;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;
import com.winnemconsulting.fbtoolkit.core.utils.ActionScriptPropertyReader;

/**
 * Pre parses a template checking for global variables such as user and date,
 * then opens a dialog with the template text that is left
 * 
 * @author Geirr Winnem
 */
public class TemplateVarParser {
	// TemplateVariables to parse
	// ${full_date} - Full date including timezone
	// ${current_date} - Current date
	// ${current_month} - Current month
	// ${current_time} - Current time
	// ${date_time} - Current date time
	// ${day_of_week} - Current day name
	// ${month_number} - Month as a number
	// ${day_of_month} - Day of month as a number
	// ${day_of_week_number} - Day of week (the week starts on Sunday)
	// ${date_time24} - DateTime24 - a 24 hour clock version of datetime.
	// ${current_year} - Current year.
	// ${current_short_year} - Current two digit year.
	// ${class_name} Class name. Computed from filename
	// ${current_file} - Current file name (just the file)
	// ${current_folder} - Current folder
	// ${current_full_path} - Current path (full file name)
	// ${current_project_path} - Just the folder
	// ${user_name} - Current user
	// ${current_project_name} - Project name the resource belongs to
	// ${current_project_root} - Project root folder on disk
	// ${package_definition} - NameSpace for class file computed from disk

	/**
	 * Constructor
	 */
	public TemplateVarParser() {
		super();
	}

	/**
	 * Parsing template file
	 * 
	 * @param strToParse
	 *            String to parse
	 * @param activeFile
	 *            File open in editor
	 * @param shell
	 *            {@link Shell}
	 * @param isClassTemplate
	 *            True if this is a class template and not a code snippet
	 *            template
	 * @return Parsed string
	 */
	public static String parse(final String strToParse, final IFile activeFile, final Shell shell, final Boolean isClassTemplate) {
		String currentFile = "";
		String currentClass = "";
		String currentFolder = "";
		String currentPath = "";
		String currentProjectPath = "";
		String currentProjectName = "";
		String currentNameSpace = "";
		String currentSourceFolder = "";

		// Root folder to project on disk
		String currentProjectRoot = "";
		if (activeFile != null) {
			try {
				String tmp = activeFile.getFileExtension();
				tmp = activeFile.getFullPath().toString(); // From projectname
				tmp = activeFile.getProjectRelativePath().toString();

				currentFile = activeFile.getName();
				currentClass = currentFile.replace("." + activeFile.getFileExtension(), "");

				currentPath = activeFile.getRawLocation().toFile().getAbsolutePath();

				currentProjectName = activeFile.getProject().getName();

				currentProjectRoot = activeFile.getProject().getLocation().toString();

				File fullPath = new File(currentPath);
				currentFolder = fullPath.getParent();
				currentProjectPath = activeFile.getParent().toString();
				String[] path = currentProjectPath.split("/");
				if (path.length > 0) {
					currentProjectPath = path[path.length - 1];
				}

				// Get your laughing gear round this little lot :)
				currentFile = currentFile.replaceAll("\\\\", "\\\\\\\\");
				currentPath = currentPath.replaceAll("\\\\", "\\\\\\\\");
				currentFolder = currentFolder.replaceAll("\\\\", "\\\\\\\\");
				currentProjectPath = currentProjectPath.replaceAll("\\\\", "\\\\\\\\");

				// Namespace
				if (isClassTemplate) {
					ActionScriptPropertyReader ac = new ActionScriptPropertyReader();
					if (ac.parsePropertyFile(activeFile.getProject().getLocation().toString())) {
						ac.loadProperties();
						currentSourceFolder = ac.getMainSourceFolderPath();
						currentNameSpace = tmp.replaceAll(currentSourceFolder + "/", "");
						currentNameSpace = currentNameSpace.replaceAll("/", ".");
						currentNameSpace = currentNameSpace.replaceAll("." + currentFile, "");
					} else {
						// FBLog.logInfo(Activator.PLUGIN_ID,
						// "Not able to extract namespace in class TemplateVarParser");
					}
				}
			} catch (Exception exception) {
				FBLog.logError(Activator.PLUGIN_ID, exception);
			}
		}

		String newStr = strToParse;

		newStr = newStr.replaceAll("\\$\\{current_file\\}", currentFile);

		newStr = newStr.replaceAll("\\$\\{class_name\\}", currentClass);

		newStr = newStr.replaceAll("\\$\\{current_project_name\\}", currentProjectName);

		newStr = newStr.replaceAll("\\$\\{current_folder\\}", currentFolder);

		newStr = newStr.replaceAll("\\$\\{current_full_path\\}", currentPath);

		newStr = newStr.replaceAll("\\$\\{current_project_path\\}", currentProjectPath);

		newStr = newStr.replaceAll("\\$\\{current_project_root\\}", currentProjectRoot);

		newStr = newStr.replaceAll("\\$\\{package_definition\\}", currentNameSpace);

		newStr = newStr.replaceAll("\\$\\{user_name\\}", System.getProperty("user.name"));
		newStr = parseDateVariables(newStr);

		// send the template string to the smart dialog
		if (isClassTemplate.equals(false)) {
			newStr = CodegenSmartDialog.parse(newStr, shell);
		}
		return newStr;
	}

	/**
	 * Parsing all date related variables in template
	 * 
	 * @return parsed string
	 */
	public static String parseDateVariables(final String strToParse) {
		Calendar calendar = new GregorianCalendar();
		Date currentTime = new Date();
		calendar.setTime(currentTime);
		String newStr = strToParse;

		newStr = newStr.replaceAll("\\$\\{full_date\\}", currentTime.toString());

		SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");
		String formattedDate = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{current_date\\}", formattedDate);

		formatter = new SimpleDateFormat("MMMM");
		String formattedMonth = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{current_month\\}", formattedMonth);

		formatter = new SimpleDateFormat("k:mm:ss a");
		String formattedTime = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{current_time\\}", formattedTime);

		formatter = new SimpleDateFormat("M/d/yyyy K:mm:ss a");
		String formattedDateTime = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{date_time\\}", formattedDateTime);

		formatter = new SimpleDateFormat("EEEE");
		String formattedDayOfWeek = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{day_of_week\\}", formattedDayOfWeek);

		formatter = new SimpleDateFormat("MM");
		String formattedMonthNumber = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{month_number\\}", formattedMonthNumber);

		formatter = new SimpleDateFormat("dd");
		String formattedDayOfMonth = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{day_of_month\\}", formattedDayOfMonth);

		newStr = newStr.replaceAll("\\$\\{day_of_week_number\\}", Integer.toString(calendar.get(Calendar.DAY_OF_WEEK)));

		formatter = new SimpleDateFormat("M/d/yyyy kk:mm:ss");
		String formattedDateTime24 = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{date_time24\\}", formattedDateTime24);

		formatter = new SimpleDateFormat("yyyy");
		String formattedYear = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{current_year\\}", formattedYear);

		formatter = new SimpleDateFormat("yy");
		String formattedYear2Digit = formatter.format(currentTime);
		newStr = newStr.replaceAll("\\$\\{current_short_year\\}", formattedYear2Digit);

		return newStr;
	}
}
