/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import org.eclipse.jface.dialogs.MessageDialog;

/**
 * Class for centralizing dialog handling.
 * 
 * @author Geirr Winnem
 * 
 */
public class MsgDialogs {

	/**
	 * Error dialog with default title.
	 * 
	 * @param message
	 *            String message to display.
	 * @see GlobalMsgs#USER_ERROR
	 */
	public static void UserError(final String message) {
		MessageDialog.openError(PlatformUtils.getShell(),
				GlobalMsgs.USER_ERROR, message);
	}

	/**
	 * Warning dialog with default title.</b> Default title is defined in
	 * {@link GlobalMsgs#USER_WARNING}
	 * 
	 * @param message
	 *            String message to display.
	 * @see GlobalMsgs#USER_WARNING
	 */
	public static void Warning(final String message) {
		MessageDialog.openWarning(PlatformUtils.getShell(),
				GlobalMsgs.USER_WARNING, message);
	}

	/**
	 * Warning dialog.</b>
	 * 
	 * @param title
	 *            title to display.
	 * @param message
	 *            message to display.
	 */
	public static void Warning(final String title, final String message) {
		MessageDialog.openWarning(PlatformUtils.getShell(), title, message);
	}

	/**
	 * Operation not allowed warning dialog.
	 * 
	 * @param message
	 *            String message to display.
	 * @see GlobalMsgs#OPERATION_NOT_ALLOWED
	 */
	public static void operationNotAllowed(final String message) {
		MessageDialog.openWarning(PlatformUtils.getShell(),
				GlobalMsgs.OPERATION_NOT_ALLOWED, message);
	}

	/**
	 * Information dialog with default title.</b> Default title is defined in
	 * {@link GlobalMsgs#USER_INFORMATION}
	 * 
	 * @param message
	 *            String message to display.
	 * @see GlobalMsgs#USER_INFORMATION
	 */
	public static void Information(final String message) {
		MessageDialog.openInformation(PlatformUtils.getShell(),
				GlobalMsgs.USER_INFORMATION, message);
	}

	/**
	 * Opens a Question dialog
	 * 
	 * @param title
	 *            Title to display
	 * @param question
	 * @return boolean True if ok is pushed otherwise false.
	 */
	public boolean YesNo(final String title, final String question) {
		return MessageDialog.openQuestion(PlatformUtils.getShell(), title,
				question);
	}
}
