/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Resource class for icons
 * 
 * @author Geirr Winnem
 * 
 */
public class IconResources {// NOPMD

	protected static URL iconBaseURL;
	public static final String TEMPLATEVIEW = "codegen";//$NON-NLS-1$

	public static final String FOLDER_ADD = "folder_add.png";//$NON-NLS-1$
	public static final String FOLDER_DELETE = "folder_delete.png";//$NON-NLS-1$
	public static final String FOLDER = "folder.png";//$NON-NLS-1$
	public static final String TEMPLATE_ADD = "template_add.png";//$NON-NLS-1$
	public static final String TEMPLATE_EDIT = "template_edit.png";//$NON-NLS-1$
	public static final String TEMPLATE_DELETE = "template_delete.png";//$NON-NLS-1$
	public static final String TEMPLATE_INSERT = "template_insert.png";//$NON-NLS-1$
	public static final String TEMPLATE_FORM = "template.png";//$NON-NLS-1$
	public static final String TEMPLATE_FILE = "XMLFile16x16.png";//$NON-NLS-1$
	public static final String VIEW_REFRESH = "refresh.png";//$NON-NLS-1$

	public static final String TASK_TAG = "tag_blue.png"; //$NON-NLS-1$
	public static final String DIALOG_INPUT = "script_edit.png"; //$NON-NLS-1$

	private static final ImageRegistry IMAGE_REGISTRY = Activator.getDefault()
			.getImageRegistry();

	public static final String TEMPLATE_ICON = "snip.gif";

	/**
	 * Default constructor
	 */
	private IconResources() {
		super();
	}

	/**
	 * 
	 */
	public static void loadResources() {
		if (iconBaseURL == null) {
			final String pathSuffix = "icons/";
			try {
				iconBaseURL = new URL(Activator.getDefault().getBundle()
						.getEntry("/"), pathSuffix);
			} catch (MalformedURLException exception) {
				FBLog.logError(exception);
			}

			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_ADD);
			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_EDIT);
			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_DELETE);
			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_INSERT);
			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_FORM);
			addImageToRegistry(TEMPLATEVIEW, TEMPLATE_FILE);
			addImageToRegistry(TEMPLATEVIEW, FOLDER_ADD);
			addImageToRegistry(TEMPLATEVIEW, FOLDER_DELETE);
			addImageToRegistry(TEMPLATEVIEW, FOLDER);
			addImageToRegistry(TEMPLATEVIEW, VIEW_REFRESH);
			addImageToRegistry(TEMPLATEVIEW, DIALOG_INPUT);
			addImageToRegistry(TEMPLATEVIEW, TASK_TAG);
		}
	}

	/**
	 * gets an image for the registry
	 */
	public static Image get(final String key) {
		return IMAGE_REGISTRY.get(key);
	}

	/**
	 * gets a handle to the registry
	 */
	public static ImageRegistry getImageRegistry() {
		return IMAGE_REGISTRY;
	}

	public static ImageDescriptor getImageDescriptor(final String offset,
			final String imageid) {

		return createDescriptor(offset, imageid);
	}

	/**
	 * add and image to the image registry
	 */
	protected static ImageDescriptor addImageToRegistry(final String offset,
			final String name) {
		try {
			final ImageDescriptor result = ImageDescriptor
					.createFromURL(createIconFileURL(offset, name));
			IMAGE_REGISTRY.put(name, result);
			return result;
		} catch (MalformedURLException e) {
			e.printStackTrace(System.err);
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}

	/**
	 * create an image descriptor from an offset and name
	 */
	protected static ImageDescriptor createDescriptor(final String offset,
			final String name) {
		try {
			return ImageDescriptor
					.createFromURL(createIconFileURL(offset, name));
		} catch (MalformedURLException e) {
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}

	/**
	 * creates a url to an icon using the base, offset and the name
	 */
	protected static URL createIconFileURL(final String offset,
			final String name) throws MalformedURLException {
		if (iconBaseURL == null) {
			throw new MalformedURLException();
		}

		if (offset == null) {
			return new URL(iconBaseURL, name);
		} else {
			final StringBuffer buffer = new StringBuffer(offset);
			buffer.append('/');
			buffer.append(name);
			return new URL(iconBaseURL, buffer.toString());
		}
	}

	/**
	 * Creates an overlay dot at the bottom right of the given image. The dot is
	 * offset at 1px from the right-most and bottom-most non-transparent pixel
	 * of the image. Evaluation of the right-most and bottom-most
	 * non-transparent pixel is performed by reading left from the center of the
	 * image y axis and up from the center of the image x axis respectively.
	 * 
	 * @param image
	 *            The image that the overlay will be added to
	 * @param color
	 *            The color that the overlay should be.
	 * @return
	 */
	public static Image addOverlay(final Image image, final RGB color) {
		final PaletteData palette = new PaletteData(0xFF, 0xFF00, 0xFF0000);
		final int overlayColor = palette.getPixel(color);
		final ImageData fullImageData = image.getImageData();
		final ImageData transparency = fullImageData.getTransparencyMask();

		int width = fullImageData.width;
		int height = fullImageData.height;
		final int midX = java.lang.Math.round(width / 2);
		final int midY = java.lang.Math.round(height / 2);
		for (int i = width - 1; i >= 0; i--) {
			final int pixelColor = fullImageData.getPixel(i, midX);
			final int transColor = transparency.getPixel(i, midY);
			if (pixelColor != transColor) {
				width = i;
				break;
			}
		}
		for (int i = height - 1; i >= 0; i--) {
			final int pixelColor = fullImageData.getPixel(midX, i);
			final int transColor = transparency.getPixel(midY, i);
			if (pixelColor != transColor) {
				height = i;
				break;
			}
		}
		final int minX = width - 10;
		final int maxX = width - 1;
		final int minY = height - 10;
		final int maxY = height - 1;
		for (int i = minX; i <= maxX; i++) {
			for (int j = minY; j <= maxY; j++) {
				boolean repaint = true;
				if (i == minX || i == maxX) {
					if (j == minY) {
						repaint = false;
					} else if (j == maxY) {
						repaint = false;
					}
				}

				if (repaint) {
					fullImageData.setPixel(i, j, overlayColor);
				}
			}
		}
		final Image fullImage = new Image(Display.getCurrent(), fullImageData);
		return fullImage;// NOPMD
	}

}