/*
Copyright (c) 2011-
Winnem Consulting
eMail: support@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Creating basic directory structure used in FBToolkit projects.
 */
public class CreateDirectoryStructureUtils implements IViewActionDelegate {

	/**
	 * Basic MVCS directory structure array used in puremvc projects
	 */
	public static final String[] PUREMVC_PATHS = {
			"controller/startup", "controller/notifications", "model/vo", "view/components", "services" }; //$NON-NLS-1$ //$NON-NLS-2$

	private ISelection selection;
	private IPath basePath;
	private IStructuredSelection ssel;

	/**
	 * Creating basic MVCS folder structure.
	 * 
	 * @param paths
	 *            String[]
	 * @throws CoreException
	 * @see org.eclipse.core.runtime.CoreException
	 */
	public void addToProjectStructure(final String[] paths)
			throws CoreException {

		for (String path : paths) {
			final IFolder etcFolders = ResourcesPlugin.getWorkspace().getRoot()
					.getFolder(basePath.append(path));
			createFolder(etcFolders);
		}
	}

	/**
	 * Creating single folder
	 * 
	 * @param folder
	 *            IFolder
	 * @throws CoreException
	 */
	private static void createFolder(final IFolder folder) throws CoreException {
		final IContainer parent = folder.getParent();
		if (parent instanceof IFolder) {
			createFolder((IFolder) parent);
		}
		if (!folder.exists()) {
			folder.create(false, true, null);
		}
	}

	public void init(IViewPart view) {
		// We dont touch this
	}

	public void run(IAction action) {
		if ((this.selection != null) && (!this.selection.isEmpty())
				&& ((this.selection instanceof IStructuredSelection))) {
			Object obj = ssel.getFirstElement();
			if ((obj instanceof IAdaptable)) {
				IResource resource = (IResource) ((IAdaptable) obj)
						.getAdapter(IResource.class);
				basePath = resource.getFullPath();
				try {
					addToProjectStructure(PUREMVC_PATHS);
				} catch (CoreException exception) {
					FBLog.logError(Activator.PLUGIN_ID, exception);
					MessageDialog.openError(PlatformUtils.getShell(),
							"Error", "Creating directories failed");
				}
			}
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
		ssel = (IStructuredSelection) this.selection;
	}
}
