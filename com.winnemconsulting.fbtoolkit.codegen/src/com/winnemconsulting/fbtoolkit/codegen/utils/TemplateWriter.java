/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.core.runtime.IPath;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceUtils;
import com.winnemconsulting.fbtoolkit.codegen.views.TemplatesView;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Class writing codegen templates
 * 
 * @author Geirr Winnem
 */
public class TemplateWriter {
	/**
	 * Warning dialog message.
	 */
	private static final String NOT_ABLE_TO_WRITE_TEMPLATE = "Not able to write template !";

	/**
	 * Template file
	 */
	File templateFile;
	File parentFolder;
	String templateType;
	String fileExtension;
	IPath templateBaseFolder;

	/**
	 * Constructor
	 * 
	 * @param parentFolder
	 * @param templateType
	 * @param templateBaseFolder
	 */
	public TemplateWriter(final File parentFolder, final String templateType,
			final IPath templateBaseFolder) {
		super();

		// if the parentFolder is null, then we are at the root surely?
		if (parentFolder == null) {
			this.parentFolder = (File) templateBaseFolder;
		} else {
			this.parentFolder = parentFolder;
		}
		this.templateType = templateType;
		fileExtension = "." + TemplatesView.TEMPLATE_EXT;
	}

	/**
	 * Deleting template
	 * 
	 * @param templateName
	 *            Name of template to delete
	 */
	private void deleteTemplate(final String templateName) {
		final File templateFile = new File(parentFolder.toString()
				+ File.separator + templateName + fileExtension);
		if (templateFile.isFile()) {
			try {
				templateFile.delete();
			} catch (SecurityException exception) {
				MsgDialogs.Warning("Security exception",
						"Delete of template failed.");
			}
		}
	}

	/**
	 * Writing the CodeTemplate
	 * 
	 * @param templateName
	 *            Name of template
	 * @param templateTrigger
	 *            Trigger text
	 * @param templateDescription
	 *            Short description of template
	 * @param codeText
	 *            Content of template(template body)
	 * @param isNewTemplateName
	 *            True if this is an existing template where name has changed
	 * @param templateOldName
	 *            Original name of template if template name has changed during
	 *            editing of template
	 */
	public void writeTemplate(final String templateName,
			final String templateTrigger, final String templateDescription,
			final String codeText, final Boolean isNewTemplateName,
			final String templateOldName) {

		// Deleting old file if template name has changed during edit
		if (isNewTemplateName) {
			deleteTemplate(templateOldName);
		}
		final File templateFile = new File(parentFolder.toString()
				+ File.separator + templateName + fileExtension);
		final String templateContents = createFormattedTemplate(templateName,
				templateDescription, codeText);
		try {

			if (!templateFile.isFile()) {
				if (templateFile.createNewFile()) {
					// MessageDialog.openInformation(PlatformUtils.getShell(),
					// "Information", "Template created");
				} else {
					MsgDialogs.Warning(NOT_ABLE_TO_WRITE_TEMPLATE);
				}
			}
			final FileWriter writer = new FileWriter(templateFile);
			writer.write(templateContents);
			writer.close();

			// Updating key combination template file so we have all the
			// triggers there
			if (templateTrigger.length() > 0) {
				String filepath = templateFile.getAbsolutePath().replaceAll(
						"\\\\", "/");
				String basePath = PreferenceUtils.getTemplatePath().replaceAll(
						"\\\\", "/");
				String relativePath = filepath.replaceFirst(basePath, "");

				PropertyFileWriter propertyFileWriter = new PropertyFileWriter(
						templateTrigger, relativePath);
				propertyFileWriter.execute();
			}
		} catch (IOException exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
		}
	}

	/**
	 * Formatting Code template file
	 * 
	 * @param templateName
	 * @param templateDescription
	 * @param startText
	 * @return Formatted template to write to disk
	 */
	private String createFormattedTemplate(final String templateName,
			final String templateDescription, final String startText) {
		String templateContents = "";
		if (templateType == TemplatesView.TEMPLATE_TYPE) {
			templateContents = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			templateContents += "<template>\n";
			templateContents += "<name>" + templateName + "</name>\n";
			templateContents += "<description>" + templateDescription
					+ "</description>\n";
			templateContents += "<code><![CDATA[" + startText + "]]></code>\n";
			templateContents += "</template>";
		}
		return templateContents;
	}

	/**
	 * Creating a new folder on disk if folder does not exit
	 * 
	 * @param folderName
	 *            String name of directory to create
	 */
	public void writeFolder(final String folderName) {
		File newFolder = new File(parentFolder.toString() + File.separator
				+ folderName);
		newFolder.mkdir();
	}

}
