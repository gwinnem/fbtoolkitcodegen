/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

/**
 * Container for global messages. Convenient way to reuse messages.
 * 
 * @author Geirr Winnem
 * 
 */
public class GlobalMsgs {

	/**
	 * Button Ok text.</br> English value = Ok
	 */
	public static final String OK = "Ok";

	/**
	 * Button cancel text.</br> English value = Cancel
	 */
	public static final String CANCEL = "Cancel";

	/**
	 * Dialog Title used when there is a information dialog.</br> English value
	 * = Information !
	 */
	public static final String USER_INFORMATION = "Information !";

	/**
	 * Dialog Title used when there is a user error.</br> English value = User
	 * Error !
	 */
	public static final String USER_ERROR = "User Error !";

	/**
	 * Dialog Title used when there is a warning.</br> English value = Warning !
	 */
	public static final String USER_WARNING = "Warning !";

	/**
	 * Dialog Title used when there is a Operation not allowed warning.</br>
	 * English value = Operation not allowed !
	 */
	public static final String OPERATION_NOT_ALLOWED = "Operation not allowed !";

}
