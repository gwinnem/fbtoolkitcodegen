/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Class used by the GenericEncloserAction and TemplateCreateCommand
 * 
 * @author Geirr Winnem
 */
public class Encloser {

	/**
	 * Wraps the selection with the startCode and endCode string
	 * 
	 * @param doc
	 *            the document this belongs to
	 * @param sel
	 *            the selection to be wrapped
	 * @param startCode
	 *            the string to put before the selection
	 * @param endCode
	 *            the string to put before the selection
	 */
	public void enclose(IDocument doc, ITextSelection sel, String start,
			String end) {
		try {
			StringBuffer cmtpart = new StringBuffer();

			int offset = sel.getOffset();
			int len = sel.getLength();

			if (start.length() <= 0 || end.length() <= 0) {
				len++;
			}

			cmtpart.append(start);

			// dont go past endCode of file.
			if (offset >= doc.getLength()) {
				len = 0;
			}

			String selection = doc.get(offset, len);
			if (len == 1 && selection.matches("[ \t\n\r]")) {
				len = 0;
			} else {
				cmtpart.append(selection);
			}
			cmtpart.append(end);

			doc.replace(offset, len, cmtpart.toString());
		} catch (BadLocationException exception) {
			FBLog.logError(Activator.PLUGIN_ID, exception);
		}
	}
}
