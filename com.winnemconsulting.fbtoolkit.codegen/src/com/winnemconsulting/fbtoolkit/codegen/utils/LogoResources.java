/*
 * Copyright (c) 2011- Winnem Consulting eMail: fbtoolkit@winnemconsulting.com
 * Web: http://www.winnemconsulting.com
 * 
 * All rights reserved.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.utils;

import java.security.InvalidParameterException;

import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.ResourceManager;

/**
 * Utility class for getting logo images
 * 
 * @author Geirr Winnem
 * 
 */
public class LogoResources {

	/**
	 * Logo image 16x16
	 */
	public final static String LOGO_16 = "icons/logo16.png";
	/**
	 * Logo image 24x24
	 */
	public final static String LOGO_24 = "icons/logo24.png";
	/**
	 * Logo image 32x32
	 */
	public final static String LOGO_32 = "icons/logo32.png";
	/**
	 * Logo image 128x128
	 */
	public final static String LOGO_128 = "icons/logo128.png";
	/**
	 * Logo image 256x256
	 */
	public final static String LOGO_256 = "icons/logo256.png";

	/**
	 * Returning logo as image
	 * 
	 * @param logoSize
	 *            size of the logo wanted
	 * @return logo
	 * @throws InvalidParameterException
	 */
	public static Image getLogo(final String logoSize)
			throws InvalidParameterException {
		Image retImage = null;
		if (logoSize.equals(LOGO_16)) {
			retImage = getImage(LOGO_16);
		} else if (logoSize.equals(LOGO_24)) {
			retImage = getImage(LOGO_24);
		} else if (logoSize.equals(LOGO_32)) {
			retImage = getImage(LOGO_32);
		} else if (logoSize.equals(LOGO_128)) {
			retImage = getImage(LOGO_128);
		} else if (logoSize.equals(LOGO_256)) {
			retImage = getImage(LOGO_256);
		} else {
			throw new InvalidParameterException("Invalid logo size");
		}
		return retImage;
	}

	private static Image getImage(final String imageName) {
		return ResourceManager.getPluginImage(
				"com.winnemconsulting.fbtoolkit.codegen", imageName);
	}
}
