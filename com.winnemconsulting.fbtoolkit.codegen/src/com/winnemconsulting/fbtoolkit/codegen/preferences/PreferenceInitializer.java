/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#
	 * initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_TEMPLATES_PATH,
				getDefaultTemplatePath());
	}

	/**
	 * Default path where code templates are stored
	 * 
	 * @return String Default templates path.
	 */
	private String getDefaultTemplatePath() {
		PreferenceUtils.checkTemplateDir();
		String retVal = getPluginStateLocation() + getPathSep()
				+ PreferenceConstants.P_TEMPLATES_PATH_EXT;
		return retVal.replaceAll("\\\\", "/");
	}

	/**
	 * Getting platform independent path separator.
	 * 
	 * @return String path separator
	 */
	public String getPathSep() {
		return System.getProperty("file.separator");
	}

	/**
	 * Plugin's working directory in the current workspace
	 * 
	 * @return String Path to working directory for plugin
	 */
	public String getPluginStateLocation() {
		String retVal = "";
		try {
			retVal = Activator.getDefault().getStateLocation().toString();
		} catch (IllegalStateException exception) {
			FBLog.logError(exception);
		}
		return retVal;
	}
}