/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.preferences;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

/**
 * Class handling changes in preferences
 * 
 * @author Geirr Winnem
 * 
 */
public class PreferenceManager {
	private final ArrayList<IPropertyChangeListener> listeners;

	public PreferenceManager() {
		super();
		listeners = new ArrayList<IPropertyChangeListener>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.preference.IPreferenceStore#addPropertyChangeListener
	 * (org.eclipse.jface.util.IPropertyChangeListener)
	 */
	public void addPropertyChangeListener(final IPropertyChangeListener listener) {
		listeners.add(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.preference.IPreferenceStore#firePropertyChangeEvent
	 * (java.lang.String, java.lang.Object, java.lang.Object)
	 */
	public void firePropertyChangeEvent(final Object srcObj, final String name,
			final Object oldValue, final Object newValue) {
		final Iterator<IPropertyChangeListener> listenerIter = this.listeners
				.iterator();
		final PropertyChangeEvent event = new PropertyChangeEvent(srcObj, name,
				oldValue, newValue);

		while (listenerIter.hasNext()) {
			final IPropertyChangeListener listener = listenerIter.next();
			listener.propertyChange(event);
		}
	}
}
