/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.preferences;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.preference.IPreferenceStore;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.utils.AbstractPropertyFile;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;
import com.winnemconsulting.fbtoolkit.core.utils.JarUtils;

/**
 * Utility class for working with preferences
 * 
 * @author Geirr Winnem
 */
public class PreferenceUtils {

	/**
	 * Checking if the defined code template directory exists. Creates directory
	 * if it is missing
	 */
	public static void checkTemplateDir() {
		String templatePath = "";
		if (getTemplatePath().equals("")) {
			templatePath = Activator.getDefault().getStateLocation().toString();
		} else {
			templatePath = getTemplatePath();
		}
		templatePath.replaceAll("\\\\", "/");
		final File fileName = new File(templatePath);
		if (!fileName.exists()) {
			fileName.mkdir();
		}
	}

	/**
	 * Path and filename to property file
	 * 
	 * @return String complete path and filename to property file
	 */
	public static String getPropfilePath() {
		return getTemplatePath() + getFileSep()
				+ AbstractPropertyFile.FILE_NAME;
	}

	private static String getFileSep() {
		return System.getProperty("file.separator");
	}

	/**
	 * Path where templates are stored
	 * 
	 * @return String complete path to where templates are stored.
	 */
	public static String getTemplatePath() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		String retVal = store.getString(PreferenceConstants.P_TEMPLATES_PATH);
		return retVal.replaceAll("\\\\", "/");
	}

	/**
	 * Returning a string preference
	 * 
	 * @param prefName
	 *            String Name of preference
	 * @return String value of reference or an empty string if preference is not
	 *         found.
	 */
	public static String getPreferenceString(final String prefName) {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		return store.getString(prefName);
	}

	/**
	 * Checking if there is a property file present. Creates a empty one if
	 * there is no file found.
	 */
	public static void propertyFileExists() {
		checkTemplateDir();
		final File propFile = new File(getPropfilePath());
		// final Properties codeTemplateProps = new Properties();
		if (!propFile.exists()) {
			copyTemplates("codetemplate.properties");
			copyTemplates("All variables example.xml");
			copyTemplates("Class fileheader.xml");
			copyTemplates("User input examples.xml");
			// try {
			// FileOutputStream propFileWriter = new
			// FileOutputStream(getPropfilePath());
			// codeTemplateProps.store(propFileWriter,
			// AbstractPropertyFile.HEADER_TEXT);
			// propFileWriter.close();
			// } catch (IOException exception) {
			// FBLog.logError(Activator.PLUGIN_ID, exception);
			// }
		}
	}

	/**
	 * Copying template files from plugin bundle to disk
	 * 
	 * @param fileName
	 *            name of the file to copy
	 */
	public static void copyTemplates(final String fileName) {
		final String destDir = getTemplatePath() + "/" + fileName;
		// final Bundle pluginBundle = Activator.getDefault().getBundle();

		final String fullPath = "templates/" + fileName;
		try {
			JarUtils.extractFile(fullPath, destDir);
		} catch (IOException e) {
			FBLog.logError(e);
		}
		// try {
		// // Starting filecopys
		//
		// URL srcFile = FileLocator.resolve(pluginBundle.getEntry(fullPath));
		// srcFile = FileLocator.toFileURL(srcFile);
		// final File outFile = new File(destDir + "/" + fileName);
		// final File inFile = new File(srcFile.getFile());
		// if (inFile.exists()) {
		// FileUtils.copyFile(inFile, outFile, FileUtils.OVERRIDE_ALWAYS);
		// } else {
		// FBLog.logInfo("Template file: " + fullPath
		// + " in bundle not found");
		// }
		// } catch (IOException e) {
		// FBLog.logError(e);
		// }
	}
}
