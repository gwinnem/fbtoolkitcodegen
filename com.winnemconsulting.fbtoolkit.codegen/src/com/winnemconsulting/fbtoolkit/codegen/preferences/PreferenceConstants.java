/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	/**
	 * Description of page
	 */
	public static final String P_PAGE_DESC = "FBToolkit Common Preferences";//$NON-NLS-1$

	/**
	 * Name of preference where templates directory is stored
	 */
	public static final String P_TEMPLATES_PATH = "templatesPathPreference";//$NON-NLS-1$
	/**
	 * Text used in DirectoryField
	 */
	public static final String P_TEMPLATES_LABEL = "Templates Directory:";//$NON-NLS-1$

	/**
	 * Sub directory where code templates will be stored
	 */
	public static final String P_TEMPLATES_PATH_EXT = "templates";//$NON-NLS-1$

}
