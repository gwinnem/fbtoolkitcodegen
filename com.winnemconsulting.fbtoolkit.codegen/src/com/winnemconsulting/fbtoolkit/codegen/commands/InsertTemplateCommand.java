/*
Copyright (c) 2011-
Winnem Consulting
eMail: fbtoolkit@winnemconsulting.com
Web:   http://www.winnemconsulting.com

All rights reserved. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS 
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED 
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER 
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.winnemconsulting.fbtoolkit.codegen.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.MultiPageEditorPart;
import org.eclipse.ui.texteditor.ITextEditor;

import com.winnemconsulting.fbtoolkit.Activator;
import com.winnemconsulting.fbtoolkit.codegen.preferences.PreferenceUtils;
import com.winnemconsulting.fbtoolkit.codegen.utils.Encloser;
import com.winnemconsulting.fbtoolkit.codegen.utils.MsgDialogs;
import com.winnemconsulting.fbtoolkit.codegen.utils.PlatformUtils;
import com.winnemconsulting.fbtoolkit.codegen.utils.PropertyFileReader;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateReader;
import com.winnemconsulting.fbtoolkit.codegen.utils.TemplateVarParser;
import com.winnemconsulting.fbtoolkit.core.logging.FBLog;

/**
 * Command for inserting generated code into editor
 */
public class InsertTemplateCommand extends AbstractHandler {

	private String templateCode = "";
	private Encloser encloser;

	/**
	 * Constructor
	 */
	public InsertTemplateCommand() {
		super();
	}

	/**
	 * Constructor
	 * 
	 * @param String
	 *            triggerText trigger text used to trigger action
	 * @param Shell
	 *            shell
	 * @see Shell
	 * 
	 */
	public InsertTemplateCommand(final String triggerText, final Shell shell) {

		final PropertyFileReader propertyFileReader = new PropertyFileReader();
		final ITextEditor editor = PlatformUtils.getActiveEditor();
		final IDocument doc = editor.getDocumentProvider().getDocument(
				editor.getEditorInput());
		final ISelection sel = editor.getSelectionProvider().getSelection();

		final String fileName = propertyFileReader
				.getCodeTemplateProperty(triggerText);

		final TemplateReader templateReader = new TemplateReader();
		IFile activeFile = null;
		if (editor.getEditorInput() instanceof IFileEditorInput) {
			activeFile = ((IFileEditorInput) editor.getEditorInput()).getFile();
		}

		templateReader.read(PreferenceUtils.getTemplatePath() + fileName);

		setTemplateCode(TemplateVarParser.parse(templateReader
				.getTemplateCodeBlock(), activeFile, shell, false));
		encloser = new Encloser();
		encloser.enclose(doc, (ITextSelection) sel, getTemplateCode(), "");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands
	 * .ExecutionEvent)
	 */
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IEditorPart editorPart = HandlerUtil.getActiveEditor(event);
		final Shell shell = editorPart.getSite().getShell();
		ITextEditor editor = null;

		if (editorPart instanceof ITextEditor) {
			editor = (ITextEditor) editorPart;
		} else if (editorPart instanceof MultiPageEditorPart) {
			editor = (ITextEditor) editorPart.getAdapter(ITextEditor.class);
		}
		ITextSelection sel = (ITextSelection) editor.getSelectionProvider()
				.getSelection();
		final IDocument doc = editor.getDocumentProvider().getDocument(
				editor.getEditorInput());

		if (editor != null && editor.isEditable()) {
			final PropertyFileReader propertyFileReader = new PropertyFileReader();

			String sequence = "";

			final int cursorOffset = (sel).getOffset();
			int lastSpaceOffset = -1;
			final FindReplaceDocumentAdapter finder = new FindReplaceDocumentAdapter(
					doc);

			try {
				final IRegion lastSpace = finder.find(cursorOffset - 1,
						"[^\\*0-9a-zA-Z_-]", false, false, false, true);

				if (lastSpace == null) {
					lastSpaceOffset = 0;
				} else {
					lastSpaceOffset = lastSpace.getOffset() + 1;
				}

				if (cursorOffset > lastSpaceOffset) {
					// ok, it could be valid, but we need to check what comes
					// after the cursor.
					if (cursorOffset != doc.getLength()) {
						IRegion nextSpace = finder.find(cursorOffset - 1,
								"[^\\*0-9a-zA-Z_-]", true, false, false, true);
						if (nextSpace != null
								&& nextSpace.getOffset() == cursorOffset) {
							sequence = doc.get().substring(lastSpaceOffset,
									cursorOffset);
						}

					} else {
						sequence = doc.get().substring(lastSpaceOffset,
								cursorOffset);
					}
				}
			} catch (Exception exception) {
				FBLog.logError(Activator.PLUGIN_ID, exception);
			}

			if (sequence.length() == 0) {
				MsgDialogs.Information("Trigger text not found");
			}

			if (sequence.length() > 0) {

				String[] stringArray = sequence.split("\\*");
				String trigger = stringArray[0];
				int loopcount = 1;
				if (stringArray.length > 1) {
					loopcount = Integer.parseInt(stringArray[1].trim());
				}

				// Here starts the actual triggering of a template using the
				// trigger text
				String fileName = propertyFileReader
						.getCodeTemplateProperty(sequence);

				TemplateReader templateReader = new TemplateReader();

				IFile activeFile = null;
				if (editor.getEditorInput() instanceof IFileEditorInput) {
					activeFile = ((IFileEditorInput) editor.getEditorInput())
							.getFile();
				}
				if (fileName == null) {
					// nasty nasty hack to tell the user no template found
					MsgDialogs.Warning("No template found for : " + sequence);
					return null;
				}

				templateReader.read(fileName);
				String template = "";
				int CursorOffset = -1;

				for (int i = 0; i < loopcount; i++) {
					setTemplateCode(TemplateVarParser.parse(templateReader
							.getTemplateCodeBlock(), activeFile, shell, false));
					if (getTemplateCode() == null) {
						template = null;
						break;
					} else {
						template = getTemplateCode();
					}

					if (template != null && template.length() > 0) {
						encloser = new Encloser();
						encloser.enclose(doc, sel, template, "");
						// move the cursor to before the endCode of the new
						// insert
						int offset = (sel).getOffset();
						offset += (sel).getLength();
						offset += template.length();
						if (i == 0) {
							try {
								doc.replace(lastSpaceOffset, sequence.length(),
										"");
								sel = new TextSelection(doc, offset
										- sequence.length(), 0);
							} catch (BadLocationException exception) {
								FBLog.logError(Activator.PLUGIN_ID, exception);
								MessageDialog.openError(PlatformUtils
										.getShell(), "Error", exception
										.getLocalizedMessage());
							}
						} else {
							sel = new TextSelection(doc, offset, 0);
						}

						editor.setHighlightRange(offset, 0, true);
					} else {
						MessageDialog.openError(PlatformUtils.getShell(),
								"Error", "No trigger specified for : "
										+ trigger + "in "
										+ PreferenceUtils.getPropfilePath());
					}
				}
				if (CursorOffset > 0) {
					editor.setHighlightRange(CursorOffset, 0, true);
				}
			}
		}
		return null;
	}

	/**
	 * @param templateCode
	 *            the Code to set
	 */
	protected void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the Code
	 */
	protected String getTemplateCode() {
		return templateCode;
	}
}
